from ngsolve import *
import time

# convcf = lambda b, f, omega, Omega: 1j*(b[0] * f.Diff(x) + b[1] * f.Diff(y)) + omega*f + 1j*Cross(Omega,f)
divcf = lambda f: f[0].Diff(x) + f[1].Diff(y)
gradcf = lambda f: CF((f.Diff(x), f.Diff(y)))
hesscf = lambda f: CF((f.Diff(x).Diff(x), f.Diff(x).Diff(y), f.Diff(x).Diff(y), f.Diff(y).Diff(y)), dims=(2,2))

n = specialcf.normal(2)
db = lambda u,b: Grad(u)*b
dbcf = lambda f,b: (b[0] * f.Diff(x) + b[1] * f.Diff(y)) 
conv = lambda u,b,omega,Omega: 1j*Grad(u)*b + omega*u #+ 1j*Cross(Omega,u)
# conv = lambda u,b,omega,Omega: Grad(u)*b - 1j*omega*u
tproj = lambda u: u - (u * n) * n
jump = lambda u: u - u.Other()
tjump = lambda u: tproj(u) - tproj(u.Other())
convavg = lambda u,b,omega,Omega: 0.5 * (conv(u,b,omega,Omega) + conv(u.Other(),b,omega,Omega))
divavg = lambda u: 0.5 * (div(u) + div(u.Other()))

def Xerror(u,ex,b,mesh):
    try:
        xerr = sqrt(Integrate( IP(div(u)-div(ex),div(u)-div(ex)).real + IP(Grad(u)*b-Grad(ex)*b,Grad(u)*b-Grad(ex)*b).real + IP(u-ex,u-ex).real , mesh ))
    except:
        xerr = sqrt(Integrate( IP(div(u)-divcf(ex),div(u)-divcf(ex)).real + IP(db(u,b)-dbcf(ex,b),db(u,b)-dbcf(ex,b)).real + IP(u-ex,u-ex).real , mesh ))
    return xerr

def conserr(u,b,omega,rho,cs,gamma,p,mesh):
    u0hess = CF((u.Operator('hesse')[0,:]),dims=(2,2))
    u1hess = CF((u.Operator('hesse')[1,:]),dims=(2,2))
    u0grad = Grad(u)[0,:]
    u1grad = Grad(u)[1,:]
    try:
        gradp = gradcf(p).Compile()
        hessp = hesscf(p).Compile()
    except:
        gradp = 0
        hessp = 0
    tgradb = CF((b[0].Diff(x),b[1].Diff(x),b[0].Diff(y),b[1].Diff(y)), dims=(2,2))
    S1 = rho * (-omega**2*u - 2*1j*omega*Grad(u)*b + CF((b*(u0hess*b + tgradb*u0grad) , b*(u1hess*b + tgradb*u1grad) ))) \
            - 1j*omega*gamma*rho*u
    S2 = CF(( (rho*cs**2).Diff(x)*div(u) + rho*cs**2*(u0hess[0,0]+u1hess[0,1]) , \
              (rho*cs**2).Diff(y)*div(u) + rho*cs**2*(u1hess[1,1]+u0hess[0,1]) )) \
            -div(u)*gradp + grad(u).trans*gradp

    l2ss = sqrt( Integrate( IP(S1-S2,S1-S2).real, mesh,definedon=mesh.Materials('outer')) )
    l2s1 = sqrt( Integrate( IP(S1,S1).real, mesh,definedon=mesh.Materials('outer')) )
    try:
        consitency_err = l2ss/l2s1
    except:
        consitency_err = None
    return consitency_err, S1, S2


IP = lambda u,v: InnerProduct(u,v)

def H1Assemble(fes,b,rhs,rho,cs,omega,Omega,gamma,p=None,p_reduction=False,bintord=5,lamb_n=100,nitsche=False,precond=None):
    c = None
    h = specialcf.mesh_size
    mesh = fes.mesh
    u,v = fes.TnT()
    if p_reduction:
        (u, pp), (v, qq) = fes.TnT()
    order = fes.globalorder

    csr=(cs**2*rho).Compile()
    try:
        gradp = gradcf(p).Compile()
        hessp = hesscf(p).Compile()
    except:
        gradp = 0
        hessp = 0

    a = BilinearForm(fes, symmetric = True)
    # conv
    a += -IP(rho * conv(u,b,omega,Omega), conv(v,b,omega,Omega)) * dx(bonus_intorder=bintord)
    # div
    a += IP(cs**2*rho * div(u) , div(v) )* dx(bonus_intorder=bintord)
    # damp
    a += - 1j*omega * IP(gamma*rho*u, v )* dx(bonus_intorder=bintord)
    if p!=None and not p_reduction:
        a += IP(div(u) , gradp * v)* dx(bonus_intorder=bintord)
        a += IP(gradp * u, div(v)) * dx(bonus_intorder=bintord)
        a += IP(hessp * u, v) * dx(bonus_intorder=bintord)
    elif p!=None:
        a += IP(pp,div(v)*csr) * dx(bonus_intorder=bintord)
        a += IP(div(u)*csr,qq) * dx(bonus_intorder=bintord)

        a += IP(pp, gradp/csr * v)* dx(bonus_intorder=bintord)
        a += IP(gradp/csr * u, qq) * dx(bonus_intorder=bintord)
        a += -2*IP(pp,qq) * dx(bonus_intorder=bintord)

        a += IP(hesscf(p) * u, v) * dx(bonus_intorder=bintord)
    # Nitsche BND term
    if nitsche:
        a += -IP(cs**2*rho * u*n, div(v) ) * ds(skeleton = True,bonus_intorder=bintord,definedon=mesh.Boundaries("bnd"))
        a += -IP(cs**2*rho * div(u), v*n ) * ds(skeleton = True,bonus_intorder=bintord,definedon=mesh.Boundaries("bnd"))
        a += lamb_n*order**2 / h * cs**2*rho * IP(v * n, u * n) * ds(skeleton = True,bonus_intorder=bintord,definedon=mesh.Boundaries("bnd"))
    if precond:
        c = Preconditioner(a, precond,complex=True)
    a.Assemble()

    f = LinearForm(fes)
    f += IP(rhs, v) * dx(bonus_intorder=bintord)
    f.Assemble()

    return a,f,c

def HdivAssemble(fes,b,rhs,rho,cs,omega,Omega,gamma,bintord=5,lamb_b=10,lamb_n=10,precond=None):
    c = None
    h = specialcf.mesh_size
    order = fes.globalorder
    u,v = fes.TnT()

    a = BilinearForm(fes, symmetric=True)
    # Anisotropic Diffusion part
    # conv
    a += -IP(rho * conv(u,b,omega,Omega), conv(v,b,omega,Omega)) * dx(bonus_intorder=bintord)
    a += IP(rho * convavg(u,b,omega,Omega), (b*n) * jump(v)) * dx(skeleton = True,bonus_intorder=bintord)
    a += IP(rho * convavg(v,b,omega,Omega), (b*n) * jump(u)) * dx(skeleton = True,bonus_intorder=bintord)
    a += -rho * lamb_b*order**2 / h * IP( (b*n)*jump(u),  (b*n)*jump(v)) * dx(skeleton = True,bonus_intorder=bintord)

    # a += rho * IP(conv(u,b,omega,Omega), (b*n) * v) * ds(skeleton = True,bonus_intorder=bintord)
    # a += rho * IP(conv(v,b,omega,Omega), (b*n) * u) * ds(skeleton = True,bonus_intorder=bintord)
    # a += -rho * lamb_b*order**2 / h * (b*n)**2 * IP((u), (v)) * ds(skeleton = True,bonus_intorder=bintord)

    # div part
    a += IP(cs**2*rho * div(u) , div(v) )* dx(bonus_intorder=bintord)
    # damp
    a += - 1j*omega * IP(gamma*rho*u, v )* dx(bonus_intorder=bintord)
    if precond:
        c = Preconditioner(a, precond,complex=True)
    a.Assemble()

    f = LinearForm(fes)
    f += IP(rhs, v) * dx(bonus_intorder=bintord)
    f.Assemble()

    return a,f,c
    
def HdivHybrid(fes,b,rhs,rho,cs,omega,Omega,gamma,p,bintord=5,hybrid=True):
    h = specialcf.mesh_size
    order = fes.globalorder
    if hybrid:
        (u,uFv,rVec),(v,vFv,sVec) = fes.TnT()
        tang = lambda u : u-(u*n)*n
        jump_u = tang(u - uFv)
        jump_v = tang(v - vFv)
    else:
        (u,rVec),(v,sVec) = fes.TnT()
        jump_u = jump(u)
        jump_v = jump(v)

    #for pressure
    try:
        gradp = gradcf(p).Compile()
        hessp = hesscf(p).Compile()
    except:
        gradp = 0
        hessp = 0

    #Initialising the bilinear form
    a = BilinearForm(fes, symmetric=False,condense=hybrid)
    if hybrid:
        dS = dx(element_boundary = True,bonus_intorder=bintord)
    else:
        dS = dx(skeleton = True,bonus_intorder=bintord) 

    prefac = 2
    #convection term
    a += -IP(rho * conv(u,b,omega,Omega), conv(v,b,omega,Omega)) * dx(bonus_intorder=bintord)

    if hybrid:
        #jump-avg terms
        a += -1j*IP(rho * conv(u,b,omega,Omega), (b*n) * jump_v ) * dS
        a += -1j*IP((b*n) * jump_u, rho*conv(v,b,omega,Omega)) * dS
        #a += -rho * lamb_b*order**2 / h * IP( (b*n)*jump_u ,  (b*n)*jump_v) * dx(element_boundary = True,bonus_intorder=bintord)

        #Lifting
        a += -prefac*IP(rho*rVec,sVec)*dx(bonus_intorder=bintord)
        a += -prefac*IP(rho*(b*n) * jump_u,sVec)*dS
        a += prefac*IP(rho*rVec,(b*n) * jump_v)*dS
    else:
        #jump-avg terms
        a += -1j*IP(rho * convavg(u,b,omega,Omega), (b*n) * jump_v ) * dS
        a += -1j*IP((b*n) * jump_u, rho * convavg(v,b,omega,Omega) ) * dS
        #a += -rho * lamb_b*order**2 / h * IP( (b*n)*jump_u ,  (b*n)*jump_v) * dx(element_boundary = True,bonus_intorder=bintord)

        #Lifting
        a += -prefac*IP(rho*rVec,sVec)*dx(bonus_intorder=bintord)
        a += -prefac*IP(rho*(b*n) * jump_u,avg(sVec))*dS
        a += prefac*IP(rho*avg(rVec),(b*n) * jump_v)*dS


    # divergence-term
    a += IP(cs**2*rho * div(u) , div(v) )* dx(bonus_intorder=bintord)

    # damping term
    a += - 1j*omega * IP(gamma*rho*u, v )* dx(bonus_intorder=bintord)
    a += IP(div(u) , gradp * v)* dx(bonus_intorder=bintord)
    a += IP(gradp * u, div(v)) * dx(bonus_intorder=bintord)
    a += IP(hessp * u, v) * dx(bonus_intorder=bintord)

    #Nitsche BND term (only for DG and if lamb_n > 0)
    if not hybrid and lamb_n > 0:
        a += -IP(cs**2*rho * u*n, div(v) ) * ds(skeleton = True,bonus_intorder=bintord,definedon=mesh.Boundaries("bnd"))
        a += -IP(cs**2*rho * div(u), v*n ) * ds(skeleton = True,bonus_intorder=bintord,definedon=mesh.Boundaries("bnd"))
        a += lamb_n*order**2 / h * cs**2*rho * IP(v * n, u * n) * ds(skeleton = True,bonus_intorder=bintord,definedon=mesh.Boundaries("bnd"))


    #Right hand side 
    f = LinearForm (fes)
    f += IP(rhs, v) * dx(bonus_intorder=bintord)


    with TaskManager():
        a.Assemble()
        f.Assemble()

    return a,f
    


import scipy.sparse as sp
import numpy as np
def ngmat2sp(m):
    rows, cols, vals = m.COO()
    return sp.csr_matrix((vals, (rows, cols))) #, dtype=np.longdouble)
def ngmat2fullsp(m):
    rows, cols, vals = m.COO()
    return sp.csr_matrix((vals, (rows, cols))).todense()


import scipy.sparse.linalg as lg
def spcond(A):
    ew1, ev = lg.eigsh(A, which='LM')
    ew2, ev = lg.eigsh(A, which='SM')
    # ew2, ev = lg.eigsh(A, sigma=1e-8)   #<--- takes a long time
    ew1 = abs(ew1)
    ew2 = abs(ew2)
    return ew1.max()/ew2.min()

import numpy.linalg as npla
def npcond(A):
    return npla.cond((A))

def infsupconst(fes,b):
    order = fes.globalorder
    mesh=fes.mesh
    pfes = L2(mesh,order=order-1,complex=True)
    u,v = fes.TnT()
    p,q = pfes.TnT()
    Sy = BilinearForm(fes)
    Sy += (IP(db(u,b),db(v,b))+IP(div(u),div(v))+IP(u,v))*dx
    Sx = BilinearForm(pfes)
    Sx += p*q*dx
    B = BilinearForm(fes,pfes)
    B += IP(div(u),q)*dx
    Sy.Assemble()
    Sx.Assemble()
    B.Assemble()
    B = ngmat2fullsp(B)
    Sy = ngmat2fullsp(Sy)
    Sx = ngmat2fullsp(Sx)
    import matplotlib.pyplot as plt
    print(B.shape)
    from scipy.linalg import svdvals,sqrtm,inv
    Su = sqrtm(inv(Sy))
    Sq = sqrtm(inv(Sx))
    M=Sq@(B@Su)
    S = svdvals(M)

    # plt.imshow(B.real)
    # plt.colorbar()
    # plt.show()
    # import pdb; pdb.set_trace()
    return min(S)
