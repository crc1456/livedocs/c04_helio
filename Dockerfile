FROM docker.gitlab.gwdg.de/crc1456/livedocs/c04_helio/c04base:latest
#FROM c04base:latest
LABEL maintainer="Pedro Costa Klein (p.klein@math.uni-goettingen.de)"

# Initial ubuntu update
USER root
RUN apt update

# Install required packages
RUN apt install -y \
        python3 \
        python3-pip

# Install dependencies
RUN python3 -m pip install --no-cache-dir notebook==6.4.12 jupyterlab jupyter_contrib_nbextensions
#RUN python3 -m pip uninstall traitlets
RUN python3 -m pip install traitlets==5.9.0

RUN pip install --no-cache-dir jupyterhub
RUN pip install voila

# Expose the port used by the jupyter notebook server
EXPOSE 8888

# create user with a home directory
ARG NB_USER="jovyan"
ARG NB_UID="1000"
ENV USER ${NB_USER}
ENV HOME /home/${NB_USER}

#Working directory
WORKDIR ${HOME}

# Copy your repo directory
COPY . ${HOME}
USER root
RUN chown -R ${NB_UID} ${HOME}
USER ${NB_USER}


# Install the requirements from requirements.txt
RUN pip install -r requirements.txt


#Install NGSolve Webgui
RUN pip3 install --user webgui_jupyter_widgets
RUN jupyter contrib nbextension install --user
RUN jupyter nbextension install --user --py webgui_jupyter_widgets
RUN jupyter nbextension enable --user --py webgui_jupyter_widgets

#Install widgets
RUN pip3 install ipywidgets
RUN jupyter nbextension enable --user --py widgetsnbextension


# Start the jupyter notebook inside the repo folder
CMD ["jupyter" , "notebook" , "--ip='0.0.0.0'" , "--port=8888", "--NotebookApp.token='crc1456livedoc'"]
	