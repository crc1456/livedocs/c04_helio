##################################################
#               Imports                          #
##################################################
from ngsolve import * 
from utils import *

##################################################
#               Preliminaries                    #
##################################################

#Inner product
IP = lambda u,v: InnerProduct(u,v)

# convcf = lambda b, f, omega, Omega: 1j*(b[0] * f.Diff(x) + b[1] * f.Diff(y)) + omega*f + 1j*Cross(Omega,f)
divcf = lambda f: f[0].Diff(x) + f[1].Diff(y)
gradcf = lambda f: CF((f.Diff(x), f.Diff(y)))
hesscf = lambda f: CF((f.Diff(x).Diff(x), f.Diff(x).Diff(y), f.Diff(x).Diff(y), f.Diff(y).Diff(y)), dims=(2,2))

#directional derivative and convection term
db = lambda u,b: Grad(u)*b
dbcf = lambda f,b: (b[0] * f.Diff(x) + b[1] * f.Diff(y))
conv = lambda u,b,omega,Omega: 1j*Grad(u)*b + omega*u

#jump, mean and convective average operators
jump = lambda u: u - u.Other()
mean = lambda u : 0.5*(u+u.Other())
convavg = lambda u,b,omega,Omega: 0.5 * (conv(u,b,omega,Omega) + conv(u.Other(),b,omega,Omega))

#for DG
avg = lambda u : 0.5*(u+u.Other())

#normal vector
n = specialcf.normal(2)


##################################################
#               Norms                            #
##################################################

#Error in the X-norm 
def Xerror(u,ex,b,mesh):
    try:
        xerr = sqrt(Integrate( IP(div(u)-div(ex),div(u)-div(ex)).real + IP(Grad(u)*b-Grad(ex)*b,Grad(u)*b-Grad(ex)*b).real + IP(u-ex,u-ex).real , mesh ))
    except:
        xerr = sqrt(Integrate( IP(div(u)-divcf(ex),div(u)-divcf(ex)).real + IP(db(u,b)-dbcf(ex,b),db(u,b)-dbcf(ex,b)).real + IP(u-ex,u-ex).real , mesh ))
    return xerr


##################################################
#               Methods                          #
##################################################

#H1-method for comparison 
#H1 method
def H1Assemble(fes,b,rhs,rho,cs,omega,Omega,gamma,p,bintord=5,lamb_n=100):
    #Extract mesh, mesh size and order from FE Space
    mesh = fes.mesh
    h = specialcf.mesh_size
    order = fes.globalorder
    #TnT functions
    u,v = fes.TnT()

    csr=(cs**2*rho).Compile()
    try:
        gradp = gradcf(p).Compile()
        hessp = hesscf(p).Compile()
    except:
        gradp = 0
        hessp = 0

    #H1 bilinear form
    a = BilinearForm(fes, symmetric = True)
    # conv
    a += -IP(rho * conv(u,b,omega,Omega), conv(v,b,omega,Omega)) * dx(bonus_intorder=bintord)
    # div
    a += IP(csr * div(u) , div(v) )* dx(bonus_intorder=bintord)

    # damp
    a += - 1j*omega * IP(gamma*rho*u, v )* dx(bonus_intorder=bintord)
    #pressure part
    a += IP(div(u) , gradp * v)* dx(bonus_intorder=bintord)
    a += IP(gradp * u, div(v)) * dx(bonus_intorder=bintord)
    a += IP(hessp * u, v) * dx(bonus_intorder=bintord)

    # Nitsche BND term
    a += -IP(csr * u*n, div(v) ) * ds(skeleton = True,bonus_intorder=bintord,definedon=mesh.Boundaries("bnd"))
    a += -IP(csr * div(u), v*n ) * ds(skeleton = True,bonus_intorder=bintord,definedon=mesh.Boundaries("bnd"))
    a += lamb_n*order**2 / h * csr * IP(v * n, u * n) * ds(skeleton = True,bonus_intorder=bintord,definedon=mesh.Boundaries("bnd"))

    #Linear form
    f = LinearForm(fes)
    f += IP(rhs, v) * dx(bonus_intorder=bintord)


    with TaskManager():
        a.Assemble()
        f.Assemble()

    return a,f



#Hdiv-conforming (H)DG 
def HdivConforming(fes,b,rhs,rho,cs,omega,Omega,gamma,p,bintord=5,hybrid=False,eliminate_hidden=False,lamb_n=1000):
    #Extracting mesh, mesh size and order from FESpace
    mesh = fes.mesh
    h = specialcf.mesh_size
    order = fes.globalorder

    #Defining the test & trial functions and jump operators 
    if hybrid:
        (u,uFv,rVec),(v,vFv,sVec) = fes.TnT()
        tang = lambda u : u-(u*n)*n
        jump_u = tang(u - uFv)
        jump_v = tang(v - vFv)
    else:
        (u,rVec),(v,sVec) = fes.TnT()
        jump_u = jump(u)
        jump_v = jump(v)

    #for pressure
    try:
        gradp = gradcf(p).Compile()
        hessp = hesscf(p).Compile()
    except:
        gradp = 0
        hessp = 0

    #Initialising the bilinear form
    a = BilinearForm(fes, symmetric=False,condense=hybrid,eliminate_hidden=eliminate_hidden)
    if hybrid:
        dS = dx(element_boundary = True,bonus_intorder=bintord)
    else:
        dS = dx(skeleton = True,bonus_intorder=bintord) 

    prefac = 2
    #convection term
    a += -IP(rho * conv(u,b,omega,Omega), conv(v,b,omega,Omega)) * dx(bonus_intorder=bintord)

    if hybrid:
        #jump-avg terms
        a += -1j*IP(rho * conv(u,b,omega,Omega), (b*n) * jump_v ) * dS
        a += -1j*IP((b*n) * jump_u, rho*conv(v,b,omega,Omega)) * dS
        #a += -rho * lamb_b*order**2 / h * IP( (b*n)*jump_u ,  (b*n)*jump_v) * dx(element_boundary = True,bonus_intorder=bintord)

        #Lifting
        a += -prefac*IP(rho*rVec,sVec)*dx(bonus_intorder=bintord)
        a += -prefac*IP(rho*(b*n) * jump_u,sVec)*dS
        a += prefac*IP(rho*rVec,(b*n) * jump_v)*dS
    else:
        #jump-avg terms
        a += -1j*IP(rho * convavg(u,b,omega,Omega), (b*n) * jump_v ) * dS
        a += -1j*IP((b*n) * jump_u, rho * convavg(v,b,omega,Omega) ) * dS
        #a += -rho * lamb_b*order**2 / h * IP( (b*n)*jump_u ,  (b*n)*jump_v) * dx(element_boundary = True,bonus_intorder=bintord)

        #Lifting
        a += -prefac*IP(rho*rVec,sVec)*dx(bonus_intorder=bintord)
        a += -prefac*IP(rho*(b*n) * jump_u,avg(sVec))*dS
        a += prefac*IP(rho*avg(rVec),(b*n) * jump_v)*dS


    # divergence-term
    a += IP(cs**2*rho * div(u) , div(v) )* dx(bonus_intorder=bintord)

    # damping term
    a += - 1j*omega * IP(gamma*rho*u, v )* dx(bonus_intorder=bintord)
    a += IP(div(u) , gradp * v)* dx(bonus_intorder=bintord)
    a += IP(gradp * u, div(v)) * dx(bonus_intorder=bintord)
    a += IP(hessp * u, v) * dx(bonus_intorder=bintord)

    #Nitsche BND term (only for DG and if lamb_n > 0)
    if not hybrid and lamb_n > 0:
        a += -IP(cs**2*rho * u*n, div(v) ) * ds(skeleton = True,bonus_intorder=bintord,definedon=mesh.Boundaries("bnd"))
        a += -IP(cs**2*rho * div(u), v*n ) * ds(skeleton = True,bonus_intorder=bintord,definedon=mesh.Boundaries("bnd"))
        a += lamb_n*order**2 / h * cs**2*rho * IP(v * n, u * n) * ds(skeleton = True,bonus_intorder=bintord,definedon=mesh.Boundaries("bnd"))


    #Right hand side 
    f = LinearForm (fes)
    f += IP(rhs, v) * dx(bonus_intorder=bintord)


    with TaskManager():
        a.Assemble()
        f.Assemble()

    return a,f

#Full (H)DG
def fullDG(fes,b,rhs,rho,cs,omega,Omega,gamma,p,lamb=100,bintord=5,hybrid=False,lamb_n=1000):
    #Extracting mesh, mesh size and order from FESpace
    mesh = fes.mesh
    h = specialcf.mesh_size
    order = fes.globalorder

    #Defining the test & trial functions and jump operators 
    if hybrid:
        (u,uF,uFv,rVec),(v,vF,vFv,sVec) = fes.TnT()
        tang = lambda u : u-(u*n)*n
        jump_u = tang(u - uFv)
        jump_v = tang(v - vFv)
    else:
        (u,rVec),(v,sVec) = fes.TnT()
        jump_u = jump(u)
        jump_v = jump(v)

    #for pressure
    try:
        gradp = gradcf(p).Compile()
        hessp = hesscf(p).Compile()
    except:
        gradp = 0
        hessp = 0

    prefac = 2

    #Bilinear form
    a = BilinearForm(fes, symmetric=False,condense=hybrid)
    if hybrid:
        dS = dx(element_boundary = True,bonus_intorder=bintord)
    else:
        dS = dx(skeleton = True,bonus_intorder=bintord) 

    #convection term
    a += -IP(rho * conv(u,b,omega,Omega), conv(v,b,omega,Omega)) * dx(bonus_intorder=bintord)

    if hybrid:
        #jump-avg terms
        a += -1j*IP(rho * conv(u,b,omega,Omega), (b*n) * jump_v ) * dS
        a += -1j*IP((b*n) * jump_u, rho*conv(v,b,omega,Omega)) * dS
        #a += -rho * lamb_b*order**2 / h * IP( (b*n)*jump_u ,  (b*n)*jump_v) * dx(element_boundary = True,bonus_intorder=bintord)

        #Lifting
        a += -prefac*IP(rho*rVec,sVec)*dx(bonus_intorder=bintord)
        a += -prefac*IP(rho*(b*n) * jump_u,sVec)*dS
        a += prefac*IP(rho*rVec,(b*n) * jump_v)*dS
    else:
        #jump-avg terms
        a += -1j*IP(rho * convavg(u,b,omega,Omega), (b*n) * jump_v ) * dS
        a += -1j*IP((b*n) * jump_u, rho * convavg(v,b,omega,Omega) ) * dS
        #a += -rho * lamb_b*order**2 / h * IP( (b*n)*jump_u ,  (b*n)*jump_v) * dx(element_boundary = True,bonus_intorder=bintord)

        #Lifting
        a += -prefac*IP(rho*rVec,sVec)*dx(bonus_intorder=bintord)
        a += -prefac*IP(rho*(b*n) * jump_u,avg(sVec))*dS
        a += prefac*IP(rho*avg(rVec),(b*n) * jump_v)*dS


    # div
    a += IP(cs**2*rho * div(u) , div(v) )* dx(bonus_intorder=bintord)

    if hybrid:
        #jump-avg terms
        a += -IP(cs**2*rho*div(u),v*n-vF*n)*dS
        a += -IP(cs**2*rho*(u*n-uF*n),div(v))*dS
        #jump-jump terms
        a += cs**2*rho *lamb*order**2 / h * IP(u*n-uF*n,v*n-vF*n)*dS
    else:
        #jump-avg terms
        a += -IP(cs**2*rho*avg(div(u)),jump_v*n)*dS
        a += -IP(cs**2*rho*(jump_u*n),avg(div(v)))*dS
        #jump-jump terms
        a += cs**2*rho *lamb*order**2 / h * IP(jump_u*n,jump_v*n)*dS

    # damping term
    a += - 1j*omega * IP(gamma*rho*u, v )* dx(bonus_intorder=bintord)
    
    a += IP(div(u) , gradp * v)* dx(bonus_intorder=bintord)
    a += IP(gradp * u, div(v)) * dx(bonus_intorder=bintord)
    a += IP(hessp * u, v) * dx(bonus_intorder=bintord)

    if hybrid:
        a += -1*IP(gradp*u,v*n-vF*n)*dS
        a += -1*IP(u*n-uF*n,gradp*v)*dS
    else:
        a += -1*IP(gradp*avg(u),jump_v*n)*dS
        a += -1*IP(jump_u*n,gradp*avg(v))*dS

        # Nitsche BND term (only for DG)
        if lamb_n > 0:
            a += -IP(cs**2*rho * u*n, div(v) ) * ds(skeleton = True,bonus_intorder=bintord,definedon=mesh.Boundaries("outer"))
            a += -IP(cs**2*rho * div(u), v*n ) * ds(skeleton = True,bonus_intorder=bintord,definedon=mesh.Boundaries("outer"))
            a += lamb_n*order**2 / h * cs**2*rho * IP(v * n, u * n) * ds(skeleton = True,bonus_intorder=bintord,definedon=mesh.Boundaries("outer"))

    f = LinearForm (fes)
    f += IP(rhs, v) * dx(bonus_intorder=bintord)

    with TaskManager():
        a.Assemble()
        f.Assemble()

    return a,f   


