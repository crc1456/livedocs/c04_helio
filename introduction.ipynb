{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "9fab3cef",
   "metadata": {},
   "source": [
    "# What is Helioseismology?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e3ef575f",
   "metadata": {},
   "source": [
    "<button type=\"button\"><a href=#overview>Jump to overview</a></button>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7c07ddc7",
   "metadata": {},
   "source": [
    "Due to its distance to earth and the extreme temperature, studying the sun poses significant challenges for scientists. Nevertheless, we want to understand the physical properties of the sun, especially since many questions, for example regardings the structure of the sun's magnetic field, remain unanswered."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ffafe5c4",
   "metadata": {},
   "source": [
    "<img src='assets/helioviewer.png' style=\"display: block; max-width: 100%; height: auto; margin: auto; float: none!important;\" width='550px' />"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cc75f6e3",
   "metadata": {},
   "source": [
    "<div align='center'>\n",
    "    The Sun with a size comparison to earth. Image credit: <a href=#sources> [1]</a>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f270c5a",
   "metadata": {},
   "source": [
    "One attemp to solve these difficulties is <i> Helioseismology</i>, which studies the interior structure of the sun through its oscillations that can be observed on the surface. That means that while we are not able to look directly into the interior of the sun, we might be able to gain insights by studying how waves propagate through the sun.  \n",
    "\n",
    "\n",
    "The field is inspired by <i> Geoseismology </i>, which studies the propagation of wave through the earth and earthquakes. Interestingly, we can observe similar phenomena on the Sun. Following a solar flare, there is an earthquake-like event that we can observe in the form of acoustic waves rippling along the Sun's surface [[4]](#sources). These events are called <i> Sunquakes</i>. Below you can find pictures and videos of these events."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d95fd9dc",
   "metadata": {},
   "source": [
    "<table><tr>\n",
    "<td> <img src=\"assets/717152main_304_ballet_earth-orig_full_0.jpg\" style=\"width: 350px; height:350px;\"/> </td>\n",
    "<td> <video width=\"350\" height=\"350\" \n",
    "           src=\"assets/outburst304_sm.m4v\"  \n",
    "           controls>\n",
    "    </video>\n",
    "</tr></table>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7268cd7f",
   "metadata": {},
   "source": [
    "<div align='center'>\n",
    "    Solar flares. Image credit: <a href=#sources> [2],[3]</a>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9dde3dbc",
   "metadata": {},
   "source": [
    "<div align='center'>\n",
    "<video width=\"900\" height=\"350\" \n",
    "           src=\"assets/ripplemovie.m4v\"  \n",
    "           controls>\n",
    "</video>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3ea42fd3",
   "metadata": {},
   "source": [
    "<div align='center'>\n",
    "    Sunquake. Image credit: <a href=#sources> [4] </a>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4529a9f2",
   "metadata": {},
   "source": [
    "Project [C04](https://www.uni-goettingen.de/en/630954.html) of the SFB 1456 aims to construct mathematical tools to advance the field of helioseismology. For instance, we want to study the equations of solar and stellar as described by Lynden-Bell and Ostriker [[5]](#sources):"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "58db05b0",
   "metadata": {},
   "source": [
    "\\begin{align}\n",
    "\\begin{aligned}\n",
    "\\rho(-i\\omega+\\partial_{\\mathbf{b}}+\\Omega\\times)^2 \\mathbf{u}\n",
    "- \\nabla(\\rho c_s^2\\operatorname{div} \\mathbf{u})\n",
    "+ (\\operatorname{div} \\mathbf{u}) \\nabla p\n",
    "%- \\nabla^{\\top}\\dislag \\cdot \\grad\\pres \n",
    "-\\nabla(\\nabla p \\cdot \\mathbf{u}) \n",
    "\\qquad&\\\\\n",
    " + \\left(\\text{Hess}(p)-\\rho \\text{Hess}(\\phi)\\right)\\mathbf{u}\n",
    "+\\gamma \\rho(- i \\omega\n",
    "% {\\color{red}+\\partial_{\\bflow}}\n",
    ") \\mathbf{u}\n",
    "+\\rho \\nabla \\psi\n",
    "= \\mathbf{f} \\quad& \\mbox{in } \\mathcal{O}\n",
    "\\end{aligned}\\\\\n",
    "-\\frac{1}{4\\pi G}\\Delta \\phi + \\operatorname{div}(\\rho \\mathbf{u}) =  0\\quad\\mbox{in }\\mathbb{R}^3&\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd48571f",
   "metadata": {},
   "source": [
    "This equation is a generalization of <i> Galbrun's equation</i>, which describes the propagation of sound when a steady background flow is present:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "382854f1",
   "metadata": {},
   "source": [
    "\n",
    "$$ \n",
    "\\begin{aligned}\n",
    "        -\\rho (\\omega + i \\partial_{\\mathbf{b}} + i\\Omega \\times)^2 \\mathbf{u} &- \\nabla(\\rho c_s^2 \\operatorname{div} \\mathbf{u}) + (\\operatorname{div} \\mathbf{u})\\nabla p \\\\\n",
    "        &-\\nabla(\\nabla p \\cdot \\mathbf{u}) + (\\operatorname{Hess}(p)-\\rho \\operatorname{Hess}(\\phi))\\mathbf{u} + \\gamma \\rho (-i\\omega)\\mathbf{u} = \\mathbf{f} \\text{ in } \\mathcal{O}\n",
    "\\end{aligned}\n",
    "$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6ea85c58",
   "metadata": {},
   "source": [
    "The followig tables list the variables and their physical interpretation:\n",
    "<table>\n",
    "    <tr> <td> $\\mathbf{u}$ <td> lagrangian pertubations of displacement </tr>\n",
    "    <tr> <td> $\\rho$ <td> density </tr>\n",
    "    <tr> <td> $\\omega$ <td> frequency </tr>\n",
    "    <tr> <td> $\\Omega$ <td> angular velocity of the frame </tr>\n",
    "    <tr> <td> $c_s$ <td> sound speed </tr>\n",
    "    <tr> <td> $p$ <td> pressure </tr>\n",
    "    <tr> <td> $\\phi$ <td> gravitational potential </tr>\n",
    "    <tr> <td> $\\gamma$ <td> damping coefficient </tr>\n",
    "    <tr> <td> $\\mathbf{f}$ <td> source term </tr>\n",
    "    <tr> <td> $\\mathbf{b}$ <td> background velocity </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8c2c91e5",
   "metadata": {},
   "source": [
    "Recently, the well-posedness of the equations of solar and stellar oscialltion and Galbrun's equation has been shown by Halla and Hohage [[6]](#sources). The next step is to construct appropriate numerical schemes to approximate the solution of Galbrun's equation. In this repository, you can find a notebook that showcases an [$H^1$-conforming discretization](./Example_H1.ipynb) that has been developed in [[7]](#sources). We also compare the method with other recent discretizations, cf. [[10,11]](#sources), and discuss their [stability](./Stability.ipynb)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f18cb88c",
   "metadata": {},
   "source": [
    "Another tool which development has been motivated by Helioseismology are <i> Learned Infinite Elements</i> [[8,9]](#sources). Time-harmonic wave equations, like Galbrun's equations or the Helmhotz problem, are often posed on unbouded domains with radiation conditions that describe how the waves behave at infitiny. For example, the Helmholtz problem is often complemented with the <i> Sommerfeld radiation condition</i>, which intuitively states that the amplitude of the waves diminishes as the waves travels away from the source. However, commonly applied techniques to impose transparent boundary conditions for numerical simulations, for instance perfectly matched layers (PML), are not applicable to inhomegeneous media that allow for strong reflection of waves like the Sun.\n",
    "\n",
    "Learned Infinite Elements have been developed to treat these cases. The key technique is to \"learn\" the matrix representations arising from the so-called DtN map, which is used to impose transparent boundary conditions, by solving an optimization problem instead of deriving them analytically. To demonstrate the method, this repository also contains the replication material from [[8,9]](#sources)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8f512cc9",
   "metadata": {},
   "source": [
    "# Overview <a id='overview'></a>\n",
    "All notebooks contained in the repository:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a5a8088e",
   "metadata": {},
   "source": [
    "### Galbrun's equation\n",
    "\n",
    "* [Example: $H^1$-conforming discretization](./Example_H1.ipynb)\n",
    "* [Discussions on stability](./Stability.ipynb)\n",
    "* [Example: Sun parameters](./Example_SunParameters.ipynb)\n",
    "\n",
    "\n",
    "### Learned Infinite Elements [[8,9]](#sources) \n",
    "\n",
    "* [learning.ipynb](./learning.ipynb)\n",
    "* [scattering.ipynb](./scattering.ipynb)\n",
    "* [point_source.ipynb](./point_source.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6394edb9",
   "metadata": {},
   "source": [
    "****\n",
    "# References\n",
    "<a id='sources'></a> \n",
    "## Image credits\n",
    "Most pictures, and many more, can be found in the [NASA Sun Galeries](https://solarsystem.nasa.gov/resources/758/brief-outburst/?category=solar-system_sun): <br> <br>\n",
    "[1] Generated with [NASA Helioviewer](https://helioviewer.org/#). <br>\n",
    "[2] Picture: [\"Solar Ballet on the sun\"](https://solarsystem.nasa.gov/resources/765/solar-ballet-on-the-sun/?category=solar-system_sun) <br>\n",
    "[3] Video: [\"Brief Outburst\"](https://solarsystem.nasa.gov/resources/758/brief-outburst/?category=solar-system_sun) <br>\n",
    "[4] Article with Video: [\"Secrets Behind Sunquakes Could Lurk Beneath the Solar Surface\"](https://www.nasa.gov/goddard/2021/feature/secrets-behind-sunquakes-could-lurk-beneath-the-solar-surface) <br>\n",
    "## Articles\n",
    "[5] D. Lynden-Bell and J. P. Ostriker. \"On the stability of differentially rotating bodies\". In: Monthly Not. Roy. Astr. Soc., 136 (1967), pp. 293-310, https://doi.org/10.1093/mnras/136.3.293. <br>\n",
    "[6] Martin Halla and Thorsten Hohage. “On the Well-posedness of the Damped\n",
    "Time-harmonic Galbrun Equation and the Equations of Stellar Oscillations”. In:\n",
    "SIAM Journal on Mathematical Analysis 53.4 (2021), pp. 4068–4095. <br>\n",
    "[7] Martin Halla, Christoph Lehrenfeld, and Paul Stocker. <i> A new T-compatibility\n",
    "condition and its application to the discretization of the damped time-harmonic\n",
    "Galbrun’s equation. </i> 2022. [ArXiv Preprint](https://arxiv.org/abs/2209.01878). <br>\n",
    "[8] Thorsten Hohage, Christoph Lehrenfeld and Janosch Preuss. <i> Learned infinite elements. </i> 2021. In: SIAM J. Sci. Comput. Vol. 45 <br>\n",
    "[9] Janosch Preuss, <i> Learned infinite elements for helioseismology. </i> 2021. PhD Thesis.  [http://hdl.handle.net/21.11130/00-1735-0000-0008-59C7-4](http://hdl.handle.net/21.11130/00-1735-0000-0008-59C7-4) <br>\n",
    "[10] Martin Halla. <i> Convergence analysis of nonconform H(div)-finite elements for the damped time-harmonic Galbrun's equation. </i> 2023. [ArXiv Preprint](https://arxiv.org/abs/2306.03496). <br>\n",
    "[11] Tim van Beeck <i> On stable discontinuous Galerkin discretizations for Galbrun's equation. </i> 2023. [Online version](https://doi.org/10.25625/KGHQWV/F6PCXK). <br>\n",
    "****\n",
    "_Written by Tim van Beeck. Published under the [MIT](https://mit-license.org/) license._"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6f6bb094",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
