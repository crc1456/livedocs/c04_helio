from netgen.meshing import *
from netgen.meshing import Mesh as netmesh
from ngsolve import Mesh as NGSMesh
from ngsolve import *
import numpy as np
from scipy.sparse import csr_matrix
from compute_pot import local_polynomial_estimator,logder
from cmath import sqrt as csqrt

atmospheric_model = "VALC"
if atmospheric_model == "VALC":
    a = 1.0 # radius of trucation boundary
else:
    a = 1.0007126

####################################################
# load solar models and compute potential
####################################################


rS,cS,rhoS,pS,_,__ = np.loadtxt("modelSinput.txt",unpack=True)
cS = (10**-2)*cS # cm/s -> m/s
rhoS = (10**3)*rhoS # g/cm3 -> kg/m3
pS = 10**-1*pS #dyn/cm^2 -> Pa (Pascal, SI unit for pressure)
# import pdb; pdb.set_trace()

if atmospheric_model == "VALC":
    print("using Model S + VAL-C")
    rV,vV,rhoV,Tv,pV  = np.loadtxt("VALCinputWithP.txt",unpack=True)

    # sound speed is not given in VAL-C model
    # calculate it from temperature using the ideal gas law
    gamma_ideal = 5/3 # adiabatic index
    mu_ideal = 1.3*10**(-3) # mean molecular weight
    R_gas = 8.31446261815324 # universal gas constant
    cV = np.sqrt(gamma_ideal*R_gas*Tv/mu_ideal)
    rhoV = (10**3)*rhoV

    # overlap interval
    rmin = rV[-1]
    rmax = rS[0]

    weightS = np.minimum(1, (rmax-rS)/(rmax-rmin))
    weightV = np.minimum(1, (rV-rmin)/(rmax-rmin))

    r = np.concatenate((rS,rV))
    ind = np.argsort(r)
    r = r[ind]
    c = np.concatenate((cS,cV))[ind]
    rho = np.concatenate((rhoS,rhoV))[ind]
    p = np.concatenate((pS,pV))[ind]
    weight = np.concatenate((weightS,weightV))[ind]
else:
    print("using Model S + Atmo")
    ind = np.argsort(rS)
    r = rS[ind]
    c = cS[ind]
    rho = rhoS[ind]
    p = pS[ind]
    weight = np.ones(len(r))


#weight = np.ones(len(weight))
#alpha_decay = 1/45

RSun =  6.963*10**8 # radius of the Sun in m
# c0 = 6.855e5 # sound speed at interface [cm/s]
# H = 1.25e7 # density scale height in [cm]
# omega_c = 0.0052*2*pi # c0/(2*H) cut-off frequency in Herz

rho_clean,drho,ddrho = logder(rho,r,weight,3)
#rho_clean = logder(rho,r,weight,1)
c_clean = logder(c,r,weight,1)
p_clean = logder(p,r,weight,1)
idx1 = np.argmin(np.abs(r - a))
alpha_infty = (-drho/rho_clean)[idx1]
c_infty = c_clean[idx1]
print("alpha_infty = ",  alpha_infty)
print("c_infty = ", c_infty)
#input("")
i0 = np.min( np.nonzero(r > 0.99)[0] )

#import pdb; pdb.set_trace()
f,df,ddf = logder(1/np.sqrt(rho),r,weight,3)
with np.errstate(divide='ignore', invalid='ignore'): #to ignore the divide by zero... it is fixed in the next line anyways...
    pot_rho = np.sqrt(rho_clean)*(ddf+2*df/r)
pot_rho[0] = pot_rho[1] # treating the NaN value at r = 0

#print("pot_rho = ", pot_rho[:20])

pot_c = c_clean.copy()


####################################################
# BSpline approximation of c,rho and potential
####################################################

spline_order = 2 # order of Spline approx. for coefficients

r = [r[0] for i in range(spline_order)] + r.tolist() + [r[-1] for i in range(spline_order)]
rho_clean = [rho_clean[0] for i in range(spline_order)] + rho_clean.tolist() + [rho_clean[-1] for i in range(spline_order)]
pot_rho= [pot_rho[0] for i in range(spline_order)] + pot_rho.tolist() + [pot_rho[-1] for i in range(spline_order)]
pot_c = [pot_c[0] for i in range(spline_order)] + pot_c.tolist() + [pot_c[-1] for i in range(spline_order)]
c_clean = [c_clean[0] for i in range(spline_order)] + c_clean.tolist() + [c_clean[-1] for i in range(spline_order)]

#for pressure
p_clean = [p_clean[0] for i in range(spline_order)] + p_clean.tolist() + [p_clean[-1] for i in range(spline_order)]

pot_rho_B = BSpline(spline_order,r,pot_rho)(x)
pot_c_B = BSpline(spline_order,r,pot_c)(x)
c_cleanB = BSpline(spline_order,r,c_clean)(x)
rho_cleanB = BSpline(spline_order,r,rho_clean)(x)

cBB = BSpline(spline_order,r,c_clean)
rhoBB =BSpline(spline_order,r,rho_clean)
pBB = BSpline(spline_order,r,p_clean)
eval_c = [cBB(pp) for pp in rS[::-1]]
eval_c[-1] = cBB(rS[::-1][-1]-1e-11)
eval_rho = [rhoBB(pp) for pp in rS[::-1]]


####################################################
# some auxiliary functions
####################################################

def sqrt_2branch(z):
    if z.real < 0 and z.imag <0:
        return - csqrt(z)
    else:
        return csqrt(z)

####################################################
# testing
####################################################

def sunmesh(maxh, minh, order):
    from netgen.geom2d import unit_square, SplineGeometry
    from ngsolve.TensorProductTools import MeshingParameters
    import math
    geo = SplineGeometry()
    geo.AddCircle((0, 0), a, bc = "outer")
    mu = 0.4

    mp = MeshingParameters (maxh = maxh)
    refpoints = int(1/minh)
    for i in range(0, refpoints+1):
        for j in range(0, refpoints+1):
            xk = (i/refpoints)*cos(j/refpoints*2*pi)
            yk = (i/refpoints)*sin(j/refpoints*2*pi)
            rr = sqrt(xk*xk+yk*yk)
            h = max(minh, maxh*math.pow(1-rr,1-mu))
            mp.RestrictH (x=xk, y=yk, z=0, h=h )

    mesh = Mesh(geo.GenerateMesh(mp=mp))
    mesh.Curve(order+2)
    return mesh 


if __name__ == "__main__":
    # from ngsolve.TensorProductTools import *
    from ngsolve import *
    import netgen.gui

    order = 5
    # rho,cs,radius = modelS(order)
    maxh = 0.3
    minh = 0.001
    mesh = sunmesh(maxh, minh, order)

    r = sqrt(x**2+y**2)
    Draw(rhoBB(r),mesh,"rho")
    Draw(cBB(r),mesh,"cs")
    input()
