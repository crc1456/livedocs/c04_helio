{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "4febf3f6",
   "metadata": {},
   "source": [
    "# Galbrun's equation: $H^1$-conforming discretization"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2b55bceb",
   "metadata": {},
   "source": [
    "<button type=\"button\"><a href=introduction.ipynb>Back to the introduction</a></button>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fb04e546",
   "metadata": {},
   "source": [
    "This notebooks contains an implementation of the $H^1$ conforming discretization of Galbrun's equation from [[1]](#sources)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "50e87106",
   "metadata": {},
   "source": [
    "We start by importing the basic functionality of the finite element library $\\texttt{ngsolve}$. Furthermore, we import two custom build scripts. The first provides functionality for barycentric mesh refinements, and the second contains the code to solve Galbrun's equation with $\\texttt{ngsolve}$. The latter is not included in this notebook to keep the presentation short and to emphasize the results, but feel free to take a deeper look at the $\\texttt{Galbrun.py}$ script if you are interested. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "33255153",
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "from math import pi\n",
    "\n",
    "\n",
    "#NGSolve related imports \n",
    "from ngsolve import *\n",
    "from netgen.geom2d import unit_square, SplineGeometry\n",
    "from netgen.geom2d import *\n",
    "from ngsolve.meshes import *\n",
    "from ngsolve import *\n",
    "\n",
    "#Setting ngsolve options\n",
    "ngsglobals.msg_level=2\n",
    "SetNumThreads(6)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "44ca4b2b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Jupyterlite settings\n",
    "\n",
    "import sys\n",
    "try:\n",
    "    if \"pyodide\" in sys.modules:\n",
    "        import piplite\n",
    "        await piplite.install('scipy')        \n",
    "except Exception as e:\n",
    "    print(e)\n",
    "    print(\"\\n\\n\")\n",
    "    print(\"####################################################################################################\")\n",
    "    print(\"# There was an error setting up the additional jupyterlite libraries.                              #\")\n",
    "    print(\"# Please refer to the above message for more details. Ignore this if you are not using Jupyterlite #\")   \n",
    "    print(\"####################################################################################################\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "34989be8",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Custom imports\n",
    "from barycenter_ref import barycenter_ref\n",
    "from Galbrun import *"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "367fa47d",
   "metadata": {},
   "source": [
    "The following code sets general parameters for our example. Specifially, we choose: <br>\n",
    "<table>\n",
    "    <tr>  <td> density <td> $\\rho$ <td> $1.5+0.2 \\cos(\\frac{\\pi}{4}x)\\cdot\\sin(\\frac{\\pi}{2}y)$</tr>\n",
    "    <tr> <td> wave number <td> $\\omega$ <td> $0.78\\cdot2\\cdot\\pi$</tr>\n",
    "    <tr> <td> angular velocity of the frame <td> $\\Omega$  <td> $\\begin{pmatrix} 0 \\\\ 0 \\end{pmatrix}$</tr>\n",
    "    <tr> <td> sound speed <td> $c_s$ <td> $\\sqrt{1.44+0.16\\cdot\\rho}$</tr>\n",
    "    <tr> <td> pressure <td> $p$ <td> $1.44 \\cdot \\rho + 0.08\\cdot \\rho^2$</tr>\n",
    "    <tr> <td> damping coefficient <td> $\\gamma$ <td> $0.1$</tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "2bca7cd2",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Density\n",
    "rho = 1.5 + 0.2*cos(pi*x/4)*sin(pi*y/2)\n",
    "#wave number\n",
    "omega = 0.78*2*pi\n",
    "#Angular velocity\n",
    "Omega = CF((0,0))\n",
    "#Sound speed\n",
    "cs = sqrt(1.44+0.16*rho)\n",
    "#Pressure\n",
    "pres=1.44*rho+0.08*rho**2\n",
    "#Damping coefficient\n",
    "gamma = 0.1"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e9c825bd",
   "metadata": {},
   "source": [
    "Now, we set the geometry for our example. Here, we choose the square $[-4,4]^2 \\subset \\mathbb{R}^2$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "a004df33",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Geometry description\n",
    "geo = SplineGeometry()\n",
    "pnts = [ (-4,-4), (4,-4), (4,4), (-4,4) ]\n",
    "pnums = [geo.AppendPoint(*p) for p in pnts]\n",
    "lbot = geo.Append ( [\"line\", pnums[0], pnums[1]],bc=\"bnd\")\n",
    "lright = geo.Append ( [\"line\", pnums[1], pnums[2]], bc=\"bnd\")\n",
    "geo.Append ( [\"line\", pnums[2], pnums[3]], bc=\"bnd\")\n",
    "geo.Append ( [\"line\", pnums[3], pnums[0]], bc=\"bnd\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "284c536f",
   "metadata": {},
   "source": [
    "The following function solves Galbrun's equation using a $H^1$ conforming discretization as introduced in [[1]](#sources). To simplify the presentation in this notebook, the definitions of the bilinear forms $a(\\cdot,\\cdot)$ and the linear form $f(\\cdot)$ are outsourced and can be found in the script $\\texttt{Galbrun.py}$, which is called by the $\\texttt{H1Assemble}$ function. This function allows you to specify the order of approximation $k$ and the parameter $\\alpha$, which scales the background flow $b$ defined by <br> <br>\n",
    "\\begin{equation}\n",
    "b = \\frac{\\alpha}{\\rho} \\begin{pmatrix} \\sin(\\pi x) \\cos(\\pi y) \\\\ -\\cos(\\pi x) \\sin(\\pi y) \\end{pmatrix}.\n",
    "\\end{equation}\n",
    "<br>\n",
    "Further, we calculate the source term such that the solution is given by <br> <br>\n",
    "\\begin{equation}\n",
    "\\frac{1}{\\rho} \\begin{pmatrix} (1+i)g \\\\ -(1+i)g \\end{pmatrix},\n",
    "\\end{equation}\n",
    "<br>\n",
    "where $g$ is the Gaussian defined by <br> <br> \n",
    "\\begin{equation}\n",
    "g(x,y) = \\sqrt{\\frac{\\log(10^6)}{\\pi}} \\exp(-\\log(10^6)(x^2+y^2)).\n",
    "\\end{equation}\n",
    "Finally, the function allows the user to specify whether a barycentric refinement should be applied to the mesh. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "2ad7e33f",
   "metadata": {},
   "outputs": [],
   "source": [
    "def Solve_Galbrun(alpha, order, bary):\n",
    "    mesh = Mesh(geo.GenerateMesh(maxh=0.5))\n",
    "    # Generating the mesh\n",
    "    if bary: \n",
    "        mesh = barycenter_ref(mesh) \n",
    "\n",
    "    #Gaussian\n",
    "    ga = sqrt(log(10**6)/pi)*exp( -log(10**6)*(x**2+y**2) )\n",
    "    b = alpha/rho * CF(( sin(pi*x)*cos(pi*y), -cos(pi*x)*sin(pi*y) ))\n",
    "    \n",
    "    # For calculating the source term\n",
    "    convcf = lambda b, f, omega, Omega: (b[0] * f.Diff(x) + b[1] * f.Diff(y)) - 1j*omega*f\n",
    "    \n",
    "    #Source term \n",
    "    s = convcf( b, CF(( 1 , 0 )) * ga, omega, Omega )\n",
    "    \n",
    "    ## Assembling the linear system\n",
    "    fes = VectorH1(mesh,order=order,dirichlet=\"\",complex=True)\n",
    "    a,f,c = H1Assemble(fes,b,s,rho,cs,omega,Omega,gamma,p=pres,p_reduction=False,nitsche=True,precond=None)\n",
    "\n",
    "    ## Solve the linear system\n",
    "    gfu = GridFunction(fes)\n",
    "    gfu.vec.data = a.mat.Inverse() * f.vec\n",
    "    \n",
    "    # Drawing the mesh and the solution\n",
    "    from ngsolve.webgui import Draw\n",
    "    print(\"------------------------------------- Barycentric refinement: {} ----------------------------------\".format(bary))\n",
    "    Draw(mesh)\n",
    "    print(\"------------------------------------- Solution with k = {} ----------------------------------------\".format(order,alpha))\n",
    "    Draw(gfu,min=-0.03, max=0.03, autoscale=True, settings={\"mesh\":False})"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c212383f",
   "metadata": {},
   "source": [
    "Now it's your turn to experiment with the parameters of the function $\\texttt{Solve_Galbrun}$ using the interactive widget below. Each time you adjust a parameter, two images will be displayed. The first image shows the mesh type, while the second image displays the numerical solution. Turn off the barycentric mesh refinement and observe the difference! You can also increase the order of the approximation and the value of $\\alpha$ to observe their effects. Below, you will find some explanations of the differences that you might observe."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "433e74a4",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "e0178387e75d4fbeb5971ca267aa70fa",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=0.1, description='alpha', max=1.0, min=0.1), Dropdown(description='ord…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "import ipywidgets as widgets\n",
    "from ipywidgets import HBox, VBox\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from IPython.display import display\n",
    "%matplotlib inline\n",
    "\n",
    "@widgets.interact(\n",
    "    alpha=(0.1,1),order=range(2,5))\n",
    "def plot(alpha=.1, order=2, bary=True):\n",
    "    Solve_Galbrun(alpha, order, bary)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f782bd9f",
   "metadata": {},
   "source": [
    "By experimenting with the interactive widget above, you may have observed the following phenomena:\n",
    "* Disabling the barycentric mesh refinement results in a decrease in the quality of the approximation. However, increasing the order of approximation to $k=4$ eliminates this difference.\n",
    "* The solution's quality degrades as the value of $\\alpha$ increases."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "857cf4ed",
   "metadata": {},
   "source": [
    "Both phenomena are explained in [1]. Specifically, the $H^1$-conforming discretization relies on the use of divergence-free finite elements of Scott-Vogelius type. However, these elements exhibit stability only for higher polynomial degrees or special meshes generated with barycentric refinements. Consequently, when barycentric refinements are disabled, the accuracy of the approximation suffers, in particular for $k = 2$. Nonetheless, increasing the polynomial degree enhances the quality of the approximation. <br> <br>\n",
    "The second observation is tied to an assumption made in [1]. In order to proof convergence of the approximation to the continuous solution, it is assumed that the Mach number of the background flow is subsonic, i.e.\n",
    "\\begin{equation}\n",
    "\\Vert c_s^{-1} b \\Vert_{\\infty} < \\beta_h \\frac{\\underline{c}_s^2 \\underline{\\rho}}{\\overline{c}_s^2. \\overline{\\rho}}\n",
    "\\end{equation}\n",
    "In the aforementioned example, the background flow is scaled by the parameter $\\alpha$. Consequently, as $\\alpha$ becomes larger, this assumption is violated, leading to a degradation in the quality of the approximation."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b4c8532f",
   "metadata": {},
   "source": [
    "****\n",
    "<a id='sources'></a> [1] Martin Halla, Christoph Lehrenfeld, and Paul Stocker. <i> A new T-compatibility\n",
    "condition and its application to the discretization of the damped time-harmonic\n",
    "Galbrun’s equation. </i> 2022. [ArXiv Preprint](https://arxiv.org/abs/2209.01878). <br>\n",
    "[2] Tilman Alemán, Martin Halla, Christoph Lehrenfeld and Paul Stocker. <i> Robust finite element discretizations for a simplified Galbrun's equation </i> 2022. In: eccomas2022, [https://www.scipedia.com/public/Aleman_et_al_2022a](https://www.scipedia.com/public/Aleman_et_al_2022a). <br>\n",
    "\n",
    "****\n",
    "_Code by Martin Halla, Christoph Lehrenfeld, Paul Stocker and Tilman Alemán. Written by Tim van Beeck. Published under the [MIT](https://mit-license.org/) license._"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d4209d6c",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
