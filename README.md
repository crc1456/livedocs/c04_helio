# Correlations of solar oscillations: modeling and inversions
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-Jupyterlab-orange)](http://livedocs.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2Fc04_helio/HEAD)
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-JupyterClassic-orange)](http://livedocs.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2Fc04_helio/HEAD?urlpath=tree)
[![Voila](https://img.shields.io/badge/CRC1456%20Binderhub-Voila-green)](http://livedocs.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2Fc04_helio/HEAD?urlpath=voila)
[![lite-badge](https://img.shields.io/badge/CRC1456-Jupyterlite-yellow)](https://crc1456.pages.gwdg.de/livedocs/c04_helio)
[![Docker](https://img.shields.io/badge/CRC1456-Dockerhub-blue)](https://gitlab.gwdg.de/crc1456/livedocs/c04_helio/container_registry/2562)

[![static-html](https://img.shields.io/badge/CRC1456-Introduction-white)](https://crc1456.pages.gwdg.de/livedocs/c04_helio/files/introduction.html)
[![static-html](https://img.shields.io/badge/CRC1456-Helio1Conf-white)](https://crc1456.pages.gwdg.de/livedocs/c04_helio/files/helio1conf.html)
[![static-html](https://img.shields.io/badge/CRC1456-Learning-white)](https://crc1456.pages.gwdg.de/livedocs/c04_helio/files/learning.html)
[![static-html](https://img.shields.io/badge/CRC1456-Point_Source-white)](https://crc1456.pages.gwdg.de/livedocs/c04_helio/files/point_source.html)
[![static-html](https://img.shields.io/badge/CRC1456-Scattering-white)](https://crc1456.pages.gwdg.de/livedocs/c04_helio/files/scattering.html)



## What is Helioseismology?

<button type="button"><a href=#overview>Jump to overview</a></button>

Due to its distance to earth and the extreme temperature, studying the sun poses significant challenges for scientists. Nevertheless, we want to understand the physical properties of the sun, especially since many questions, for example regardings the structure of the sun's magnetic field, remain unanswered.

<img src='assets/helioviewer.png' style="display: block; max-width: 100%; height: auto; margin: auto; float: none!important;" width='550px' />

<div align='center'>
    The Sun with a size comparison to earth. Image credit: <a href=#sources> [1]</a>
</div>

One attemp to solve these difficulties is <i> Helioseismology</i>, which studies the interior structure of the sun through its oscillations that can be observed on the surface. That means that while we are not able to look directly into the interior of the sun, we might be able to gain insights by studying how waves propagate through the sun.  


The field is inspired by <i> Geoseismology </i>, which studies the propagation of wave through the earth and earthquakes. Interestingly, we can observe similar phenomena on the Sun. Following a solar flare, there is an earthquake-like event that we can observe in the form of acoustic waves rippling along the Sun's surface [[4]](#sources). These events are called <i> Sunquakes</i>. Below you can find pictures and videos of these events.

<table><tr>
<td> <img src="assets/717152main_304_ballet_earth-orig_full_0.jpg" style="width: 350px; height:350px;"/> </td>
<td> <video width="350" height="350" 
           src="assets/outburst304_sm.m4v"  
           controls>
    </video>
</tr></table>

<div align='center'>
    Solar flares. Image credit: <a href=#sources> [2],[3]</a>
</div>

<div align='center'>
<video width="900" height="350" 
           src="assets/ripplemovie.m4v"  
           controls>
</video>
</div>

<div align='center'>
    Sunquake. Image credit: <a href=#sources> [4] </a>
</div>

Project [C04](https://www.uni-goettingen.de/en/630954.html) of the SFB 1456 aims to construct mathematical tools to advance the field of helioseismology. For instance, we want to study the equations of solar and stellar as described by Lynden-Bell and Ostriker [[5]](#sources):

$$
\begin{align}
\begin{aligned}
    \rho(-i\omega+\partial_{b}+\Omega\times)^2 u 
    - \nabla(\rho c_s^2\div u)
    + (\div u) \nabla p
    %- \nabla^{\top}\dislag \cdot \grad\pres 
    -\nabla(\nabla p \cdot u) 
    \qquad &\newline
     + \left(\text{Hess}(p)-\rho \text{Hess}(\phi)\right)u
    + \gamma \rho(- i \omega
    % {\color{red}+\partial_{\bflow}}
    ) u
        +\rho \nabla \psi
        = f \quad& \text{in } \mathcal{O} 
\end{aligned} &\newline 
    -\frac{1}{4\pi G}\Delta \phi + \div(\rho u) =  0\quad\text{in }\mathbb{R}^3&
\end{align}
$$

This equation is a generalization of <i> Galbrun's equation</i>, which describes the propagation of sound when a steady background flow is present:


$$ 
\begin{aligned}
        -\rho (\omega + i \partial_b + i\Omega \times)^2 u &- \nabla(\rho c_s^2 \div u) + (\div u)\nabla p \newline
        &-\nabla(\nabla p \cdot u) + (\text{Hess}(p)-\rho \text{Hess}(\phi))u + \gamma \rho (-i\omega)u = f \text{ in } \mathcal{O}
\end{aligned}
$$


The followig tables list the variables and their physical interpretation:

|     |                                                |
| --- | ---------------------------------------------- |
| $$u$$      | lagrangian pertubations of displacement |
| $$\rho$$   | density                                 |
| $$\omega$$ | frequency                               |
| $$\Omega$$ | angular velocity of the frame           |
| $$c_s$$    | sound speed                             |
| $$p$$      | pressure                                |
| $$\phi$$   | gravitational potential                 |
| $$\gamma$$ | damping coefficient                     |
| $$f$$      | source term                             |
| $$b$$      | background velocity                     |


Recently, the well-posedness of the equations of solar and stellar oscialltion and Galbrun's equation has been shown by Halla and Hohage [[6]](#sources). The next step is to construct appropriate numerical schemes to approximate the solution of Galbrun's equation. In this repository, you can find a notebook that showcases an [$H^1$-conforming discretization](./GalbrunH1conf.ipynb) that has been developed recently [[7]](#sources).

Another tool which development has been motivated by Helioseismology are <i> Learned Infinite Elements</i> [[8,9]](#sources). Time-harmonic wave equations, like Galbrun's equations or the Helmhotz problem, are often posed on unbouded domains with radiation conditions that describe how the waves behave at infitiny. For example, the Helmholtz problem is often complemented with the <i> Sommerfeld radiation condition</i>, which intuitively states that the amplitude of the waves diminishes as the waves travels away from the source. However, commonly applied techniques to impose transparent boundary conditions for numerical simulations, for instance perfectly matched layers (PML), are not applicable to inhomegeneous media that allow for strong reflection of waves like the Sun.

Learned Infinite Elements have been developed to treat these cases. The key technique is to "learn" the matrix representations arising from the so-called DtN map, which is used to impose transparent boundary conditions, by solving an optimization problem instead of deriving them analytically. This repository contais the following notebooks describing different aspects of Learned Infinite Elements: 
* [learning.ipynb](./learning.ipynb): describes ...
* [scattering.ipynb](./scattering.ipynb): describes ...
* [point_source.ipynb](./point_source.ipynb): describes ...

## Overview <a id='overview'></a>
All notebooks contained in the repository:

### Galbrun's equation

* [GalbrunH1conf.ipynb](./GalbrunH1conf.ipynb)


### Learned Infinite Elements 

* [learning.ipynb](./learning.ipynb)
* [scattering.ipynb](./scattering.ipynb)
* [point_source.ipynb](./point_source.ipynb)

****
## References
<a id='sources'></a> 
## Image credits
Most pictures, and many more, can be found in the [NASA Sun Galeries](https://solarsystem.nasa.gov/resources/758/brief-outburst/?category=solar-system_sun): <br> <br>
[1] Generated with [NASA Helioviewer](https://helioviewer.org/#). <br>
[2] Picture: ["Solar Ballet on the sun"](https://solarsystem.nasa.gov/resources/765/solar-ballet-on-the-sun/?category=solar-system_sun) <br>
[3] Video: ["Brief Outburst"](https://solarsystem.nasa.gov/resources/758/brief-outburst/?category=solar-system_sun) <br>
[4] Article with Video: ["Secrets Behind Sunquakes Could Lurk Beneath the Solar Surface"](https://www.nasa.gov/goddard/2021/feature/secrets-behind-sunquakes-could-lurk-beneath-the-solar-surface) <br>
## Articles
[5] D. Lynden-Bell and J. P. Ostriker. "On the stability of differentially rotating bodies". In: Monthly Not. Roy. Astr. Soc., 136 (1967), pp. 293-310, https://doi.org/10.1093/mnras/136.3.293. <br>
[6] Martin Halla and Thorsten Hohage. “On the Well-posedness of the Damped
Time-harmonic Galbrun Equation and the Equations of Stellar Oscillations”. In:
SIAM Journal on Mathematical Analysis 53.4 (2021), pp. 4068–4095. <br>
[7] Martin Halla, Christoph Lehrenfeld, and Paul Stocker. <i> A new T-compatibility
condition and its application to the discretization of the damped time-harmonic
Galbrun’s equation. </i> 2022. [ArXiv Preprint](https://arxiv.org/abs/2209.01878). <br>
[8] Thorsten Hohage, Christoph Lehrenfeld and Janosch Preuss. <i> Learned infinite elements. </i> 2021. In: SIAM J. Sci. Comput. Vol. 45 <br>
[9] Janosch Preuss, <i> Learned infinite elements for helioseismology. </i> 2021. PhD Thesis.  [http://hdl.handle.net/21.11130/00-1735-0000-0008-59C7-4](http://hdl.handle.net/21.11130/00-1735-0000-0008-59C7-4) 
****
_Written by Tim van Beeck. Published under the [MIT](https://mit-license.org/) license._


## Contact us

In case you need any support, please contat us at `crc1456-inf-private at uni-goettingen.de` or `p.klein at math.uni-goettingen.de`.
