{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "4febf3f6",
   "metadata": {},
   "source": [
    "# Stable approximations of Galbrun's equation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2b55bceb",
   "metadata": {},
   "source": [
    "<button type=\"button\"><a href=introduction.ipynb>Back to the introduction</a></button>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "39ecd86f-09a0-46a3-96e9-07329e3c4aa1",
   "metadata": {},
   "source": [
    "The stability of approximations of Galbrun's equation is closely related to the stability of approximations of the Stokes problem, cf. [[1]](#sources). In particular, we require that our discrete spaces provide an inf-sup condition for the divergence operator. Thus, the following methods have been considered to discretize Galbrun's equation so far: \n",
    "\n",
    " <ul>\n",
    "  <li> In <a href=#sources> [2] </a> an $H^1$-conforming discretization has been introduced. To achieve stability, a sufficiently high polynomial degree or / and special types of meshes (barycentric refinement) are required. </li>\n",
    "  <li> In <a href=#sources> [3] </a> an $H(\\operatorname{div})$-conforming discontinuous Galerkin discretization was considered. In constrast to the $H^1$-conforming method, this method achieves stability without restrictions on the polynomial degree or special meshes. </li>\n",
    "  <li> Finally, a fully discontinous Galerkin discretization has been considered in <a href=#sources> [4] </a>. As the $H(\\operatorname{div})$-conforming, the method is stable. In this work, hybridzation has been discussed computationally to ease the computational costs. </li>\n",
    "</ul> \n",
    "\n",
    "We note that all these methods require the Mach number of the background flow $\\mathbf{b}$ the be suitably bounded. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fb04e546",
   "metadata": {},
   "source": [
    "The aim of this notebook is to demonstrate the effects that different discretizations have on the stability of the approximation. We mainly compare the $H^1$-conforming discretization which is also discussed in a [previous notebook](./Example_H1.ipynb) with a hybrid $H(\\operatorname{div})$-conforming discretization as in [[3,4]](#sources)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "50e87106",
   "metadata": {},
   "source": [
    "We use the same computational example from the [$H^1$-notebook](./Example_H1.ipynb). Check it out if you want to refresh some details. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "33255153",
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "from math import pi\n",
    "\n",
    "\n",
    "#NGSolve related imports \n",
    "from ngsolve import *\n",
    "from netgen.geom2d import unit_square, SplineGeometry\n",
    "from netgen.geom2d import *\n",
    "from ngsolve.meshes import *\n",
    "from ngsolve import *\n",
    "\n",
    "#Setting ngsolve options\n",
    "ngsglobals.msg_level=2\n",
    "SetNumThreads(6)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "44ca4b2b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Jupyterlite settings\n",
    "\n",
    "import sys\n",
    "try:\n",
    "    if \"pyodide\" in sys.modules:\n",
    "        import piplite\n",
    "        await piplite.install('scipy')        \n",
    "except Exception as e:\n",
    "    print(e)\n",
    "    print(\"\\n\\n\")\n",
    "    print(\"####################################################################################################\")\n",
    "    print(\"# There was an error setting up the additional jupyterlite libraries.                              #\")\n",
    "    print(\"# Please refer to the above message for more details. Ignore this if you are not using Jupyterlite #\")   \n",
    "    print(\"####################################################################################################\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "34989be8",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Custom imports\n",
    "from barycenter_ref import barycenter_ref\n",
    "from methods import * "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "2bca7cd2",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Density\n",
    "rho = 1.5 + 0.2*cos(pi*x/4)*sin(pi*y/2)\n",
    "#wave number\n",
    "omega = 0.78*2*pi\n",
    "#Angular velocity\n",
    "Omega = CF((0,0))\n",
    "#Sound speed\n",
    "cs = sqrt(1.44+0.16*rho)\n",
    "#Pressure\n",
    "pres=1.44*rho+0.08*rho**2\n",
    "#Damping coefficient\n",
    "gamma = 0.1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "a004df33",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Geometry description\n",
    "geo = SplineGeometry()\n",
    "pnts = [ (-4,-4), (4,-4), (4,4), (-4,4) ]\n",
    "pnums = [geo.AppendPoint(*p) for p in pnts]\n",
    "lbot = geo.Append ( [\"line\", pnums[0], pnums[1]],bc=\"bnd\")\n",
    "lright = geo.Append ( [\"line\", pnums[1], pnums[2]], bc=\"bnd\")\n",
    "geo.Append ( [\"line\", pnums[2], pnums[3]], bc=\"bnd\")\n",
    "geo.Append ( [\"line\", pnums[3], pnums[0]], bc=\"bnd\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "24e16f45-48fe-4f1d-bb31-bdbe5a61df68",
   "metadata": {},
   "source": [
    "Now we implement the $H^1$-conforming method for a given mesh and polynomial order. The weak formulation itself is imported from the script $\\texttt{methods.py}$ to avoid cluttering. Here we mainly specify the finite element space to be $[\\mathbb{P}^k(\\mathcal{T}_h)]^d \\cap [H^1]^d$ and solve the linear system. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "83ba0be7-df04-4521-9376-d1a3202ca26b",
   "metadata": {},
   "outputs": [],
   "source": [
    "def SolveH1(mesh, order): \n",
    "    #Gaussian\n",
    "    ga = sqrt(log(10**6)/pi)*exp( -log(10**6)*(x**2+y**2) )\n",
    "    alpha = 0.1\n",
    "    b = alpha/rho * CF(( sin(pi*x)*cos(pi*y), -cos(pi*x)*sin(pi*y) ))\n",
    "    \n",
    "    # For calculating the source term\n",
    "    convcf = lambda b, f, omega, Omega: (b[0] * f.Diff(x) + b[1] * f.Diff(y)) - 1j*omega*f\n",
    "    \n",
    "    #Source term \n",
    "    s = convcf( b, CF(( 1 , 0 )) * ga, omega, Omega )\n",
    "    \n",
    "    ## Assembling the linear system\n",
    "    fes = VectorH1(mesh,order=order,dirichlet=\"\",complex=True)\n",
    "\n",
    "    a,f = H1Assemble(fes,b,s,rho,cs,omega,Omega,gamma,p=pres,bintord=5,lamb_n=100)\n",
    "\n",
    "    ## Solve the linear system\n",
    "    gfu = GridFunction(fes)\n",
    "    gfu.vec.data = a.mat.Inverse() * f.vec\n",
    "\n",
    "    # Drawing the mesh and the solution\n",
    "    from ngsolve.webgui import Draw\n",
    "    print(\"------------------------------------- Solution with k = {} ----------------------------------------\".format(order,alpha))\n",
    "    Draw(gfu,min=-0.03, max=0.03, autoscale=True, settings={\"mesh\":False})"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ee0a72dc-d453-4013-aa26-a6a0da78592b",
   "metadata": {},
   "source": [
    "To compare the different methods, we also implement the $H(\\operatorname{div})$-conforming hybrid DG method. Now, the finite element space of interest is $\\mathbb{BMD}^{k}(\\mathcal{T}_h) \\times [\\mathbb{P}^k(\\mathcal{F}_h)]^d$. We note that this discretization uses a lifting operator that is implemented via a mixed formulation. Thus, we require another variable in $[\\mathbb{P}^k(\\mathcal{T}_h)]^d$. For more information on the lifting operator and its implementation we refer to [[3,4]](#sources). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "f9f2344c-3273-4375-a92f-b17d8332e4cf",
   "metadata": {},
   "outputs": [],
   "source": [
    "def SolveHdiv(mesh,order): \n",
    "    #Gaussian\n",
    "    ga = sqrt(log(10**6)/pi)*exp( -log(10**6)*(x**2+y**2) )\n",
    "    alpha = 0.1\n",
    "    b = alpha/rho * CF(( sin(pi*x)*cos(pi*y), -cos(pi*x)*sin(pi*y) ))\n",
    "    \n",
    "    # For calculating the source term\n",
    "    convcf = lambda b, f, omega, Omega: (b[0] * f.Diff(x) + b[1] * f.Diff(y)) - 1j*omega*f\n",
    "    \n",
    "    #Source term \n",
    "    s = convcf( b, CF(( 1 , 0 )) * ga, omega, Omega )\n",
    "    \n",
    "    ## Assembling the linear system\n",
    "    fes = Compress(HDiv(mesh,complex=True,order=order,dgjumps=False,dirichlet=\".*\"))\n",
    "    Fvec = TangentialFacetFESpace (mesh, order = order,complex=True,dirichlet=\".*\")\n",
    "    lift_vec = VectorL2(mesh,complex=True,order=order)\n",
    "    fes = fes * Fvec* lift_vec\n",
    "    a, f = HdivConforming(fes,b,s,rho,cs,omega,Omega,gamma,pres,bintord=5,hybrid=True,eliminate_hidden=True)\n",
    "\n",
    "    ## Solve the linear system\n",
    "    gfu = GridFunction(fes)\n",
    "    #gfu.vec.data = a.mat.Inverse() * f.vec\n",
    "\n",
    "    with TaskManager():\n",
    "        if a.condense == True:\n",
    "            fmod = (f.vec + a.harmonic_extension_trans * f.vec).Evaluate()\n",
    "            inv = a.mat.Inverse(fes.FreeDofs(True))\n",
    "        else:\n",
    "            fmod = f.vec\n",
    "            inv = a.mat.Inverse(fes.FreeDofs())\n",
    "        gfu.vec.data = inv * fmod\n",
    "\n",
    "        if a.condense == True:\n",
    "            gfu.vec.data += a.harmonic_extension * gfu.vec\n",
    "            gfu.vec.data += a.inner_solve * f.vec\n",
    "    \n",
    "\n",
    "    gfu = gfu.components[0]\n",
    "    # Drawing the mesh and the solution\n",
    "    from ngsolve.webgui import Draw\n",
    "    print(\"------------------------------------- Solution with k = {} ----------------------------------------\".format(order,alpha))\n",
    "    Draw(gfu,min=-0.03, max=0.03, autoscale=True, settings={\"mesh\":False})"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1a9fcc22-f86d-465a-9b8e-7c196db12b84",
   "metadata": {},
   "source": [
    "Now let us compare both methods with each other. We start with a coarser mesh with mesh size $h = 0.5$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "0051cfdc-c45a-4fcf-8f81-03c9cd08286a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " Generate Mesh from spline geometry\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "19634776a63c41d9ae323d552afe48de",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "WebGuiWidget(layout=Layout(height='50vh', width='100%'), value={'gui_settings': {}, 'ngsolve_version': '6.2.24…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "BaseWebGuiScene"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from ngsolve.webgui import Draw\n",
    "mesh = Mesh(geo.GenerateMesh(maxh=0.5))\n",
    "Draw(mesh)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "74cb85e6-460d-4da4-a075-000cfa51f018",
   "metadata": {},
   "source": [
    "If we use both methods to solve Galbrun's equation with polynomial degree $k = 2$, we clearly see that the $H^1$-conforming method is unstable while the $H(\\operatorname{div})$-conforming method seems to be stable. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "885aac02-e17c-4e81-a8cf-106751c2db85",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "------------------------------------- Solution with k = 2 ----------------------------------------\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "35274fca16eb472183f891a7ac85ff69",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "WebGuiWidget(layout=Layout(height='50vh', width='100%'), value={'gui_settings': {'mesh': False}, 'ngsolve_vers…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "------------------------------------- Solution with k = 2 ----------------------------------------\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "59e22660c1e84d66815beeae83f94139",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "WebGuiWidget(layout=Layout(height='50vh', width='100%'), value={'gui_settings': {'mesh': False}, 'ngsolve_vers…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "SolveH1(mesh,2)\n",
    "SolveHdiv(mesh,2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5f4608ac-69a6-40c7-8a54-b7372f8b9249",
   "metadata": {},
   "source": [
    "This effect remains (though less pronouced) when we decrease the mesh size to $h = 0.1$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "dd3b4664-07d2-4690-8f1e-1fbf59a253a4",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " Generate Mesh from spline geometry\n",
      "------------------------------------- Solution with k = 2 ----------------------------------------\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "85fe75f288c34cd2a994f387cfc9a772",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "WebGuiWidget(layout=Layout(height='50vh', width='100%'), value={'gui_settings': {'mesh': False}, 'ngsolve_vers…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "------------------------------------- Solution with k = 2 ----------------------------------------\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "fc777362b87a4ddaab5f9c0e3793193b",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "WebGuiWidget(layout=Layout(height='50vh', width='100%'), value={'gui_settings': {'mesh': False}, 'ngsolve_vers…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "mesh = Mesh(geo.GenerateMesh(maxh=0.1))\n",
    "SolveH1(mesh,2)\n",
    "SolveHdiv(mesh,2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2c659858-eaaa-413f-83df-de1181a38f62",
   "metadata": {},
   "source": [
    "In contrast, both methods work well if the polynomial degree is high enough, e.g. $k = 5$, or if barycentric refinements are used. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "ad7460d4-45d2-4e7f-8755-bc321acda84d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " Generate Mesh from spline geometry\n",
      "------------------------------------- Solution with k = 5 ----------------------------------------\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "796546793231466e8e15031517936439",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "WebGuiWidget(layout=Layout(height='50vh', width='100%'), value={'gui_settings': {'mesh': False}, 'ngsolve_vers…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "------------------------------------- Solution with k = 5 ----------------------------------------\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "4d62bbcbbe514bf7adff6208974f235f",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "WebGuiWidget(layout=Layout(height='50vh', width='100%'), value={'gui_settings': {'mesh': False}, 'ngsolve_vers…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "mesh = Mesh(geo.GenerateMesh(maxh=0.5))\n",
    "SolveH1(mesh,5)\n",
    "SolveHdiv(mesh,5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "81cdb05e-cef9-48d2-a7c5-bc80ada7fe35",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " Generate Mesh from spline geometry\n",
      "------------------------------------- Solution with k = 2 ----------------------------------------\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "50ac08a217934f06a1575bbcabc22c87",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "WebGuiWidget(layout=Layout(height='50vh', width='100%'), value={'gui_settings': {'mesh': False}, 'ngsolve_vers…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "------------------------------------- Solution with k = 2 ----------------------------------------\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "c57f31ac1255485c9f895b4e2002ea65",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "WebGuiWidget(layout=Layout(height='50vh', width='100%'), value={'gui_settings': {'mesh': False}, 'ngsolve_vers…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "mesh = Mesh(geo.GenerateMesh(maxh=0.5))\n",
    "mesh = barycenter_ref(mesh)\n",
    "SolveH1(mesh,2)\n",
    "SolveHdiv(mesh,2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b4c8532f",
   "metadata": {},
   "source": [
    "****\n",
    "<a id='sources'></a> [1] Tilman Alemán, Martin Halla, Christoph Lehrenfeld and Paul Stocker. <i> Robust finite element discretizations for a simplified Galbrun's equation </i> 2022. In: eccomas2022, [https://www.scipedia.com/public/Aleman_et_al_2022a](https://www.scipedia.com/public/Aleman_et_al_2022a). <br>\n",
    "[2] Martin Halla, Christoph Lehrenfeld, and Paul Stocker. <i> A new T-compatibility\n",
    "condition and its application to the discretization of the damped time-harmonic\n",
    "Galbrun’s equation. </i> 2022. [ArXiv Preprint](https://arxiv.org/abs/2209.01878). <br>\n",
    "[3] Martin Halla. <i> Convergence analysis of nonconform H(div)-finite elements for the damped time-harmonic Galbrun's equation. </i> 2023. [ArXiv Preprint](https://arxiv.org/abs/2306.03496). <br>\n",
    "[4] Tim van Beeck <i> On stable discontinuous Galerkin discretizations for Galbrun's equation. </i> 2023. [Online version](https://doi.org/10.25625/KGHQWV/F6PCXK). <br>\n",
    "\n",
    "****\n",
    "_Code by Martin Halla, Christoph Lehrenfeld, Paul Stocker, Tilman Alemán and Tim van Beeck. Written by Tim van Beeck. Published under the [MIT](https://mit-license.org/) license._"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8d01d6a0-a2ad-4ac8-a8b1-4b21645c78c9",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
