import numpy as np
from math import sqrt,exp
from scipy.special import hankel1,h1vp
import ceres_dtn as opt # import the dtn minimization module 
 
omega = 16 # wavenumber
a = 1.0 # coupling radius
Lmax = 72 # number of modes
alpha_decay = 2/3 # decay of weights
Nmax = 7 # maximal number of infinite element dofs
Ns = list(range(1,Nmax+1))
ansatz = "minimalIC" # ansatz for the learned IE matrices 
# corresponds to the "reduced" ansatz in the paper  

# reference dtn function (for 2D Helmholtz with radial symmetry) 
def dtn_ref(nu):
    return -omega*h1vp(nu,omega*a) / hankel1(nu,omega*a)

lam = np.array([(l/a)**2 for l in range(Lmax)]) # eigenvalues
dtn_nr = np.array([ dtn_ref(sqrt(lami*a)) for lami in lam ]) # dtn numbers (samples at eigenvalues)
weights = np.array([10**6*np.exp(-l*alpha_decay) for l in range(Lmax)]) # weights in misfit function 

l_dtn = opt.learned_dtn(lam,dtn_nr,weights**2) # create the learned dtn object 

# reuse learned matrices of dimension (N-1)x(N-1) as initial guess 
# for minimization for dimension N x N
def new_initial_guess(A_old,B_old,ansatz): 
    N = A_old.shape[0]
    A_guess = np.zeros((N+1,N+1),dtype='complex')
    B_guess = np.zeros((N+1,N+1),dtype='complex')
    A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
    A_guess[:N,:N] = A_old[:]
    B_guess[:N,:N] = B_old[:]
    A_guess[N,N] = -100-100j
    B_guess[0,N] = 1e-3*(np.random.rand(1) + 1j*np.random.rand(1)) 
    return A_guess,B_guess

# some flags for ceres 
flags = {"max_num_iterations":5000000,
         "use_nonmonotonic_steps":True,
         "minimizer_progress_to_stdout":True,
         "num_threads":4,
         "report_level":"Brief",
         "function_tolerance":1e-6,
         "parameter_tolerance":1e-8}


#initial guess for N = 1
np.random.seed(123)
A_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1) 
B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
A_IE = [None] # for storing the learned IE matrices 
B_IE = [None] 
# run the minimization
for N in Ns: 
    l_dtn.Run(A_guess,B_guess,ansatz,flags)
    A_IE.append(A_guess.copy()), B_IE.append(B_guess.copy())
    A_guess,B_guess = new_initial_guess(A_IE[N],B_IE[N],ansatz)

