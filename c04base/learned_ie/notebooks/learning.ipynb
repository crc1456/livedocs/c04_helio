{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Learned IE: Solving the minimization problem \n",
    "\n",
    "This is the first supplementary notebook for the paper [1] about learned infinite elements. It\n",
    "demonstrates how the minimization problem is solved and visualizes the resulting approximation \n",
    "of the DtN function. Two different kinds of exterior domains are treated:\n",
    "* A) Homogeneous medium. \n",
    "* B) Piecewise constant medium with a step discontinuity.\n",
    "\n",
    "[1] Learned infinite elements (Hohage, Lehrenfeld, Preuß)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Most of the imports are standard except the module `ceres_dtn`, which provides the functionality for solving the minimization problem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from math import sqrt,exp\n",
    "np.random.seed(123)\n",
    "from scipy.special import hankel1,h1vp,jv,yv,jvp,yvp\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib import gridspec\n",
    "import ceres_dtn as opt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We set some graphic options to improve the presentation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.core.display import display, HTML\n",
    "display(HTML(\"<style>.container { width:80% !important; }</style>\"))\n",
    "plt.rc('legend', fontsize=16)\n",
    "plt.rc('axes', titlesize=16)\n",
    "plt.rc('axes', labelsize=16)\n",
    "#%matplotlib notebook\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A) Homogeneous medium\n",
    "\n",
    "In this problem a homogeneous medium in the exterior of a disk with radius $r = a$ is treated: \n",
    "$$ - \\Delta u - k^2 u = 0, \\quad r \\geq a, $$\n",
    "for some constant $k > 0$. The parameters are chosen below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "k = 16\n",
    "a = 1.0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The main idea of our method is to learn an (efficient) exterior discretization which represents the DtN operator as good as possible. \n",
    "The reference DtN operator is assumed to diagonalize in the basis of the eigenfunctions of the Laplace-Beltrami operator on the coupling boundary. Also the eigenvalues of the continuous eigenpairs \n",
    "$$ - \\Delta u_{\\ell} = \\lambda_{\\ell} u_{\\ell}, \\quad \\lambda_{\\ell} = (\\ell / a)^2 $$ \n",
    "are needed for implementation of the method. The diagonal \n",
    "elements of the DtN in this eigenbasis are given by\n",
    "$$ \\text{dtn}^{\\text{ref}}(\\lambda) = - k \\frac{ (H^{(1)}_{ a \\sqrt{\\lambda   }})^{\\prime}(k a) }{ (H^{(1)}_{a \\sqrt{\\lambda   }})(k a) } , $$\n",
    "for $\\lambda = \\lambda_{\\ell}$ with the Hankel functions $H^{(1)}_{a \\sqrt{\\lambda } }$ of the first kind of order $ a \\sqrt{\\lambda}$. The continuous DtN function is defined below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def dtn_ref(nu):\n",
    "    return -k*h1vp(nu,k*a) / hankel1(nu,k*a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next it has to be decided how many of the eigenvalues should be considered for approximating the continuous DtN function. This is controlled by the parameter $\\ell = 0, \\ldots, $ `Lmax`. Furthermore, each of the continuous DtN numbers is associated with a weight factor $ w_{\\ell} \\sim \\exp(- \\ell \\alpha) $ that controls how much emphasis should be placed on fitting this particular mode. The decay of the weights is controlled by the parameter `alpha_decay`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Lmax = 72\n",
    "alpha_decay = 2/3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the arrays for the eigenvalues `lam`, the DtN numbers `dtn_nr` and the weights `weights` are set up. Additionally, we define an empty array `final_res`,\n",
    "which will later be passed to the minimization routine to collect the final residuals."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lam = np.array([(l/a)**2 for l in range(Lmax)]) \n",
    "dtn_nr = np.array([ dtn_ref(sqrt(lami)*a) for lami in lam ]) \n",
    "weights = np.array([10**6*np.exp(-l*alpha_decay) for l in range(Lmax)])\n",
    "final_res = np.zeros(len(lam),dtype=float)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These arrays are used to initialize an instance of the learned DtN class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "l_dtn = opt.learned_dtn(lam,dtn_nr,weights**2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The instance `l_dtN` has a method called `Run` which solves the minimization problem  \n",
    "$$ \\min_{A,B} \\sum\\limits_{\\ell}{   \\vert w_{\\ell}( \\text{dtn}^{\\text{ref}}(\\lambda_{\\ell}) -  \\text{dtn}(\\lambda_{\\ell}) )   \\vert^2}. $$\n",
    "To this end, the method requires initial guesses `A_guess, B_guess` for the `N+1x N+1` matrices $A$ and $B$, which are chosen as follows. For `N = 0` we simply start with a random guess while for `N > 1` the minimizer from step `N-1` is recycled. This is implemented in the function `new_initial_guess`below. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def new_initial_guess(A_old,B_old,ansatz):\n",
    "    N = A_old.shape[0]\n",
    "    A_guess = np.zeros((N+1,N+1),dtype='complex')\n",
    "    B_guess = np.zeros((N+1,N+1),dtype='complex')\n",
    "    if ansatz in [\"medium\",\"full\"]:\n",
    "        A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))\n",
    "        B_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))\n",
    "        A_guess[:N,:N] = A_old[:]\n",
    "        B_guess[:N,:N] = B_old[:]\n",
    "        A_guess[N,N] = 1.0\n",
    "    elif ansatz == \"minimalIC\":\n",
    "        A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))\n",
    "        A_guess[:N,:N] = A_old[:]\n",
    "        B_guess[:N,:N] = B_old[:]\n",
    "        A_guess[N,N] = -100-100j\n",
    "        B_guess[0,N] = 1e-3*(np.random.rand(1) + 1j*np.random.rand(1)) \n",
    "    return A_guess,B_guess"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The minimization will be run for $N = 0, \\ldots,$ `Nmax`-1. For later use we define some lists `Lone,Ltwo` and `relative_residuals` which are used to store the obtained matrices and relative residuals $ \\vert \\text{dtn}^{\\text{ref}}(\\lambda_{\\ell}) -  \\text{dtn}(\\lambda_{\\ell}) \\vert / \\vert \\text{dtn}^{\\text{ref}}(\\lambda_{\\ell}) \\vert $ for each $N$. \n",
    "\n",
    "Furthermore, we have to define the `ansatz`for $\\text{dtn}$. The learned DtN function is in general of the form \n",
    "$$ \\text{dtn}(\\lambda_{\\ell}) =   \\left( S_{00}^{\\ell}  - \\sum\\limits_{i=1}^{N } \\sum\\limits_{j=1}^{ N }  S_{0 i}^{\\ell} \\left[ \\left(    S^{\\ell}_{EE} \\right)^{-1} \\right]_{i-1 ,j-1} S_{j 0}^{\\ell} \\right), \\quad (*) $$\n",
    "with  $S^{\\ell} :=  A +\\lambda_{\\ell}  B$. \n",
    "\n",
    "The following choices for `ansatz` are currently supported:\n",
    "* `full`: For this choice the matrices $A,B$ are full matrices with no specific structure. For this choice one has to work with \n",
    "the complicated ansatz shown above for the minimization.\n",
    "* `medium`: In this case the block of the matrices that describes the exterior is assumed to be diagonal. This allows to work with the simplified \n",
    "ansatz $$  \\text{dtn}(\\lambda)  = A_{00} + \\lambda B_{00} - \\sum\\limits_{j=1}^{N}{ \\frac{(A_{0j} + \\lambda B_{0j}) ( A_{j0} + \\lambda B_{j0} )  }{  A_{jj} + \\lambda B_{jj} }     }.  $$ \n",
    "* `minimalIC`: A small additional reduction is obtained by setting some entries of $B$ equal to one\n",
    "$$  \\text{dtn}(\\lambda)  = A_{00} + \\lambda B_{00} - \\sum\\limits_{j=1}^{N}{ \\frac{(A_{0j} + \\lambda B_{0j}) ( A_{j0} + \\lambda  )  }{  A_{jj} + \\lambda  }     }.  $$\n",
    "\n",
    "The behavior of the solver can be controlled by passing a dictionary called `flags`. The meaning of the options is described [here](http://ceres-solver.org/nnls_solving.html#solver-options)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Nmax = 7\n",
    "A_IE = []\n",
    "B_IE = [] \n",
    "relative_residuals = []\n",
    "ansatz = \"minimalIC\"\n",
    "#ansatz = \"medium\"\n",
    "flags = {\"max_num_iterations\":5000000,\n",
    "         \"use_nonmonotonic_steps\":True,\n",
    "         \"minimizer_progress_to_stdout\":False,\n",
    "         \"num_threads\":4,\n",
    "         \"report_level\":\"Brief\",\n",
    "         \"function_tolerance\":1e-6,\n",
    "         \"parameter_tolerance\":1e-8}\n",
    "A_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)\n",
    "B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the mininization loop over $N$ is run. During the call `Run` the initial guesses `l1_guess,l2_guess` are overwritten \n",
    "with the obtained minimizers. For each $N$ the solver provides a short report about the number of required iterations as well as the \n",
    "initial and final costs. The latter gives a good indicator to assess if the minimization has been successfull. If the solver should ever terminate with `NO CONVERGENCE` it is useful to set the option `\"report_level\":\"Full\"` \n",
    "in the flags. A more detailed report is then provided which may help to identify the issue."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for N in range(Nmax): \n",
    "    l_dtn.Run(A_guess,B_guess,ansatz,flags,final_res)\n",
    "    A_IE.append(A_guess.copy()), B_IE.append(B_guess.copy()),relative_residuals.append(final_res.copy())\n",
    "    A_guess,B_guess = new_initial_guess(A_IE[N],B_IE[N],ansatz)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To check if the minimization has been successfull it makes sense to evaluate the learned dtn function `dtn_learned` \n",
    "at some sample points `lam_sample` and compare to to the reference. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def dtn_learned(A_IE,B_IE,lami):\n",
    "    n_L,_ = A_IE.shape\n",
    "    S_lam = A_IE + lami*B_IE\n",
    "    result = S_lam[0,0]\n",
    "    if n_L > 1:\n",
    "        invS_lam = np.linalg.inv(S_lam[1:,1:]) \n",
    "        for i in range(1,n_L):\n",
    "            for j in range(1,n_L):\n",
    "                result -= S_lam[0,i]*invS_lam[i-1,j-1]*S_lam[j,0]\n",
    "    return result\n",
    "\n",
    "lam_sample = np.linspace(0.5,1000,num=200)\n",
    "dtn_ref_sampled = np.array([ dtn_ref(sqrt(lami)*a) for lami in lam_sample])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Moreover, for a more qualitative check the relative residuals $ \\vert \\text{dtn}^{\\text{ref}}(\\lambda_{\\ell}) -  \\text{dtn}(\\lambda_{\\ell}) \\vert / \\vert \\text{dtn}^{\\text{ref}}(\\lambda_{\\ell}) \\vert, \\ell = 0, \\ldots, $ `Lmax` for different $N$ are also plotted in the left Figure. The real and imaginary part of the analytic DtN \n",
    "are compared to the learned DtN function for different $N$ in the middle and the right Figure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(24, 8)) \n",
    "gs = gridspec.GridSpec(1, 3) \n",
    "ax0 = plt.subplot(gs[0])\n",
    "plt.title(\"Relative residuals\")\n",
    "plt.xlabel(\"$\\ell$\")\n",
    "ax1 = plt.subplot(gs[1])\n",
    "plt.xlabel(\"$\\lambda$\")\n",
    "plt.title(\"Real part\")\n",
    "ax2 = plt.subplot(gs[2])\n",
    "plt.xlabel(\"$\\lambda$\")\n",
    "plt.title(\"Imaginary part\")\n",
    "\n",
    "ax1.plot(lam_sample, dtn_ref_sampled.real,linewidth=8,color='lightgray')\n",
    "ax2.plot(lam_sample, dtn_ref_sampled.imag,label=\"ref\",linewidth=8,color='lightgray')\n",
    "for N in range(Nmax):\n",
    "    if N % 2 == 0:\n",
    "        dtn_approx = np.array([dtn_learned(A_IE[N],B_IE[N],lami) for lami in lam_sample])\n",
    "        ax0.semilogy(np.arange(Lmax),relative_residuals[N])\n",
    "        ax1.plot(lam_sample, dtn_approx.real)\n",
    "        ax2.plot(lam_sample, dtn_approx.imag,label=\"$N$ = {0}\".format(N))\n",
    "        plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The so callled poles are given by those complex numbers $\\lambda$ for which $A_{EE}+\\lambda B_{EE}$ is not invertible (here $E$ represents exterior degrees of freedom). Notice that \n",
    "at these $\\lambda$ the formula (*) breaks down. Below these poles are computed as the negative eigenvalues of $  \\left[ B_{EE} \\right]^{-1} A_{EE}$ and plotted for the different $N$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "colors = ['b', 'c', 'y', 'm', 'r']\n",
    "marker = ['*','o','s','x','.']\n",
    "fig, ax = plt.subplots(figsize=(8,8))\n",
    "for N,clr,mkr in zip(range(2,Nmax),colors,marker):\n",
    "    poles = -np.linalg.eigvals(np.linalg.inv(B_IE[N][1:,1:]) @ A_IE[N][1:,1:])\n",
    "    ax.scatter(poles.real,poles.imag,s=100, c=clr, label=\"$N$ = {0}\".format(N),marker=mkr,\n",
    "               alpha=1.0, edgecolors='none')\n",
    "ax.legend()\n",
    "ax.grid(True)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## B) Piecewise constant medium with a step discontinuity."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us get ready to tackle a more complicated problem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy.special import jv,yv,jvp,yvp\n",
    "A_IE = []\n",
    "B_IE = []\n",
    "relative_residuals = []"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This problem involves an inhomogeneous exterior domain. The wavenumber has a step \n",
    "discontinuity defined by:\n",
    "\n",
    "$$ k(r) =  \\begin{cases}  \n",
    "k_{I} & r < R_{\\infty}, \\\\\n",
    "k_{\\infty} & r > R_{\\infty},\n",
    "\\end{cases}$$ \n",
    "for some $ R_{\\infty} > a$ where $a$ is again the radius of the coupling boundary. The parameters can be chosen below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 1.0 \n",
    "R_inf = 2.0 \n",
    "k_I = 16\n",
    "k_inf = 8\n",
    "alpha_decay = 1.5 "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To determine the DtN numbers \n",
    "for each mode $\\nu$ one has to solve\n",
    "\n",
    "$$ -\\frac{1}{r} \\frac{\\partial}{\\partial r} \\left( r \\frac{\\partial u_{\\nu} }{\\partial r} \\right) +  \\left( \\omega^2(r) - \\frac{\\nu^2}{r^2} \\right)u_{\\nu} = 0 \\quad r > a,$$\n",
    "with boundary condition $u_{\\nu}(a) = 1$ and radiation condition at infinity. The solution is of the form \n",
    "\n",
    "$$\n",
    "u_{\\nu}(r) = \\begin{cases}\n",
    "A_{\\nu} J_{\\nu}(k_{I}r) + B_{\\nu} Y_{\\nu}(k_I r) & r < R_{\\infty}, \\\\\n",
    "C_{\\nu} H_{\\nu}^{(1)}(k_{\\infty}r) & r > R_{\\infty},\n",
    "\\end{cases}\n",
    "$$ \n",
    "\n",
    "with constants $A_{\\nu}, B_{\\nu}$ and $C_{\\nu}$ to be determined by the boundary condition at $r = a$ and \n",
    "continuity requirements at $r = R_{\\infty}$. This leads to the system of equations\n",
    "\n",
    "$$ A_{\\nu} J_{\\nu}(k_I a ) + B_{\\nu} Y_{\\nu}(k_I a ) = 1 .$$ \n",
    "$$ A_{\\nu} J_{\\nu}(k_I R ) + B_{\\nu} Y_{\\nu}(k_I R ) - C_{\\nu} H_{\\nu}^{(1)}(k_{\\infty} R) = 0. $$ \n",
    "$$ A_{\\nu} k_I  J_{\\nu}^{\\prime}(k_I R ) + B_{\\nu} k_I Y_{\\nu}^{\\prime}(k_I R ) - C_{\\nu} k_{\\infty} (H_{\\nu}^{(1)})^{\\prime}(k_{\\infty} R) = 0. $$ \n",
    "\n",
    "A calculation shows that \n",
    "$$A_{\\nu} =  (1-B_{\\nu} Y_{\\nu}(k_I a )) /  J_{\\nu}(k_I a ), $$\n",
    "$$ B_{\\nu} =  \\frac{1}{M_{\\nu}} \\left[ \\frac{k_{\\infty}}{k_I} (H_{\\nu}^{(1)})^{\\prime}(k_{\\infty} R) \\frac{J_{\\nu}(k_I R )}{ J_{\\nu}(k_I a ) } -    H_{\\nu}^{(1)}(k_{\\infty} R) \\frac{ J_{\\nu}^{\\prime}(k_I R )}{ J_{\\nu}(k_I a )  } \\right],    $$\n",
    "for a certain $M_{\\nu}$ defined in the code below.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def M_nu(nu):\n",
    "    tmp1  = -(k_inf/k_I)*(h1vp(nu,k_inf*R_inf)/jv(nu,k_I*a))*( jv(nu,k_I*a)*yv(nu,k_I*R_inf) - yv(nu,k_I*a)*jv(nu,k_I*R_inf) )\n",
    "    tmp2 = (hankel1(nu,k_inf*R_inf)/jv(nu,k_I*a))*( yvp(nu,k_I*R_inf)*jv(nu,k_I*a) - yv(nu,k_I*a)*jvp(nu,k_I*R_inf))\n",
    "    return tmp1+tmp2\n",
    "\n",
    "def B_nu(nu):\n",
    "    tmp1 = (k_inf/k_I)*h1vp(nu,k_inf*R_inf)*jv(nu,k_I*R_inf)/jv(nu,k_I*a) \n",
    "    tmp2 = -hankel1(nu,k_inf*R_inf)*jvp(nu,k_I*R_inf)/jv(nu,k_I*a)\n",
    "    return (tmp1+tmp2)/M_nu(nu)\n",
    "\n",
    "def A_nu(nu):\n",
    "    return (1-B_nu(nu)*yv(nu,k_I*a))/jv(nu,k_I*a)\n",
    "\n",
    "def C_nu(nu):\n",
    "    tmp1 = ( yvp(nu,k_I*R_inf)*jv(nu,k_I*a) - yv(nu,k_I*a)*jvp(nu,k_I*R_inf) )*( jv(nu,k_I*R_inf) / jv(nu,k_I*a)**2)\n",
    "    tmp2 = -( jv(nu,k_I*a)*yv(nu,k_I*R_inf) - yv(nu,k_I*a)*jv(nu,k_I*R_inf) )*( jvp(nu,k_I*R_inf) / jv(nu,k_I*a)**2   )\n",
    "    return (tmp1+tmp2)/M_nu(nu)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We check that the coefficients defined above indeed solve the system\n",
    "\n",
    "$$ \\begin{bmatrix}\n",
    " J_{\\nu}(k_I a ) & Y_{\\nu}(k_I a ) & 0    \\\\\n",
    " J_{\\nu}(k_I R ) & Y_{\\nu}(k_I R ) & - H_{\\nu}^{(1)}(k_{\\infty} R)    \\\\\n",
    " \\omega_I  J_{\\nu}^{\\prime}(k_I R ) & k_I Y_{\\nu}^{\\prime}(k_I R ) & -  k_{\\infty} (H_{\\nu}^{(1)})^{\\prime}(k_{\\infty} R) \\\\\n",
    " \\end{bmatrix} \n",
    " \\begin{bmatrix}\n",
    " A_{\\nu} \\\\\n",
    " B_{\\nu} \\\\\n",
    " C_{\\nu}\n",
    " \\end{bmatrix} \n",
    " = \n",
    "  \\begin{bmatrix}\n",
    " 1 \\\\\n",
    " 0  \\\\\n",
    " 0\n",
    " \\end{bmatrix}, \n",
    " $$\n",
    " for $\\nu = 0,1, \\ldots$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def Check(nu):\n",
    "    M = np.array( [ [jv(nu,k_I*a),yv(nu,k_I*a),0],\n",
    "                    [jv(nu,k_I*R_inf),yv(nu,k_I*R_inf),-hankel1(nu,k_inf*R_inf)],\n",
    "                    [k_I*jvp(nu,k_I*R_inf),k_I*yvp(nu,k_I*R_inf),-k_inf*h1vp(nu,k_inf*R_inf)]\n",
    "                ])\n",
    "    print( \"m = {0}, diff = {1}\".format(nu, np.linalg.norm( M @ np.array([A_nu(nu),B_nu(nu),C_nu(nu)]) - np.array([1,0,0])) ))\n",
    "\n",
    "for nu in range(20):\n",
    "    Check(nu)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The reference DtN function is then given by:\n",
    "$$ \\text{dtn}^{\\text{ref}}(\\nu) = -\\frac{\\partial u_{\\nu}(a)}{\\partial r } = - k_I \\left[ A_{\\nu} J_{\\nu}^{\\prime}(k_I a )  + B_{\\nu} Y_{\\nu}^{\\prime}(k_I a )  \\right]. $$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def dtn_ref(nu):\n",
    "    return -k_I*( A_nu(nu)*jvp(nu,k_I*a) + B_nu(nu)*yvp(nu,k_I*a))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similar as above we define the arrays `lam`, `dtn_nr` and `weight`, create an instance of the learned DtN and solve the \n",
    "minimization problem using the `Run` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lam =  np.array([(mm/a)**2 for mm in range(Lmax)]) \n",
    "dtn_nr = np.array([ -k_I*( A_nu(sqrt(lami)*a)*jvp(sqrt(lami)*a,k_I*a) + B_nu(sqrt(lami)*a)*yvp(sqrt(lami)*a,k_I*a))  for lami in lam ]) \n",
    "weights = np.array([10**6*exp(-mm/alpha_decay) for mm in range(len(lam))])\n",
    "l_dtn = opt.learned_dtn(lam,dtn_nr,weights**2)\n",
    "\n",
    "A_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)\n",
    "B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)\n",
    "\n",
    "for N in range(Nmax): \n",
    "    l_dtn.Run(A_guess,B_guess,ansatz,flags,final_res)\n",
    "    A_IE.append(A_guess.copy()), B_IE.append(B_guess.copy()),relative_residuals.append(final_res.copy())\n",
    "    A_guess,B_guess = new_initial_guess(A_IE[N],B_IE[N],ansatz)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us again visualize the results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lam_sample = np.linspace(0.5,800,num=10000)\n",
    "dtn_ref_sampled = np.array([ dtn_ref(sqrt(lami)*a) for lami in lam_sample])\n",
    "\n",
    "fig = plt.figure(figsize=(24, 8)) \n",
    "gs = gridspec.GridSpec(1, 3) \n",
    "ax0 = plt.subplot(gs[0])\n",
    "plt.title(\"Relative residuals\")\n",
    "plt.xlabel(\"$\\ell$\")\n",
    "ax1 = plt.subplot(gs[1])\n",
    "plt.xlabel(\"$\\lambda$\")\n",
    "plt.title(\"Real part\")\n",
    "ax2 = plt.subplot(gs[2])\n",
    "plt.xlabel(\"$\\lambda$\")\n",
    "plt.title(\"Imaginary part\")\n",
    "\n",
    "ax1.plot(lam_sample, dtn_ref_sampled.real,linewidth=8,color='lightgray')\n",
    "ax2.plot(lam_sample, dtn_ref_sampled.imag,label=\"ref\",linewidth=8,color='lightgray')\n",
    "for N in range(Nmax):\n",
    "    if N % 2 == 0:\n",
    "        dtn_approx = np.zeros(len(lam_sample),dtype=complex)\n",
    "        opt.eval_dtn_fct(A_IE[N],B_IE[N],lam_sample,dtn_approx)\n",
    "        ax0.semilogy(np.arange(Lmax),relative_residuals[N])\n",
    "        ax1.plot(lam_sample, dtn_approx.real)\n",
    "        ax2.plot(lam_sample, dtn_approx.imag,label=\"$N$ = {0}\".format(N))\n",
    "        plt.legend(loc='upper right')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And also plot the corresponding poles."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "colors = ['b', 'c', 'y', 'm', 'r']\n",
    "marker = ['*','o','s','x','.']\n",
    "fig, ax = plt.subplots(figsize=(8,8))\n",
    "for N,clr,mkr in zip(range(3,Nmax),colors,marker):\n",
    "    poles = -np.linalg.eigvals(np.linalg.inv(B_IE[N][1:,1:]) @ A_IE[N][1:,1:])\n",
    "    ax.scatter(poles.real,poles.imag,s=100, c=clr, label=\"$N$ = {0}\".format(N),marker=mkr,\n",
    "               alpha=1.0, edgecolors='none')\n",
    "ax.legend()\n",
    "ax.grid(True)\n",
    "\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
