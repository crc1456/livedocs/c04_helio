{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Scattering of a plane wave from a disk"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook demonstrates how the learned infinite elements can be used to realize an infinite \n",
    "domain in a simple scattering problem. Consider a plane wave $g = \\exp{( i k x) }$ which is \n",
    "incident on a disk with radius $R = 1/2$. The equation for the scattered pressure is then given by\n",
    "\n",
    "$$ \\begin{align}\n",
    " - \\Delta u - k^2 u &= 0 \\quad \\text{in } r > R,  \\\\\n",
    "     u &=g \\quad \\text{at } r = R,\n",
    "  \\end{align}\n",
    "$$\n",
    "\n",
    "with radiation condition at infinity.\n",
    "\n",
    "![problem_geometry](plane-wave-sketch.png \"Sketch of problem geometry\")\n",
    "\n",
    "To treat this problem with the finite element method\n",
    "the infinite domain has to be truncated to a finite computational domain. To this end, we\n",
    "consider an annulus $ \\Omega = \\{ R < r < a \\}$. The mesh for this domain is created below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import ceres_dtn as opt\n",
    "\n",
    "import os \n",
    "import psutil \n",
    "process = psutil.Process(os.getpid())\n",
    "\n",
    "from ngsolve import *\n",
    "from ngsolve.webgui import *\n",
    "from ngsolve.solvers import BVP\n",
    "from netgen.geom2d import SplineGeometry\n",
    "from ngs_refsol import PlaneWaveHomSolution\n",
    "\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from scipy.special import hankel1,h1vp\n",
    "np.random.seed(seed=123)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 1.0\n",
    "R = 1/2\n",
    "geo = SplineGeometry()\n",
    "geo.AddCircle( (0,0), a, leftdomain=1, rightdomain=0,bc=\"outer-bnd\")\n",
    "geo.AddCircle( (0,0), R, leftdomain=0, rightdomain=1,bc=\"inner-bnd\")\n",
    "geo.SetMaterial(1, \"inner\")\n",
    "mesh = Mesh(geo.GenerateMesh (maxh=0.05,quad_dominated=False))\n",
    "mesh.Curve(10)\n",
    "Draw(mesh)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have a finite domain with a mesh. To obtain an approximate solution which converges to the solution of the true problem (posed on an infinite domain) as the discretization is refined a transparent boundary condition \n",
    "at the coupling boundary $r = a$ has to be imposed. In the framework of infinite elements (IEs) this is achieved \n",
    "by adding an additional block \n",
    "$$ \n",
    "A \\otimes M  + B \\otimes K \\quad (1)\n",
    "$$\n",
    "to the linear system which consist of \n",
    "* mass $M$ and stiffness matrix $K$ on the coupling interface $\\Gamma = \\{ r = a \\}$,\n",
    "* and infinite element matrices $A$ and $B$.\n",
    "\n",
    "The idea to learn the infinite element matrices to obtain an optimal approximation of the \n",
    "DtN operator gives our method its name. Let us now first fix the wavenumber and learn these\n",
    "matrices. Since this works exactly as explained in the notebook [learning.ipynb:](./learning.ipynb) we\n",
    "just reproduce the code here without further comments."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "k = 16\n",
    "Lmax = 100\n",
    "alpha_decay = 2/3\n",
    "Nmax = 7\n",
    "r_ext = min(R*k/16,R)\n",
    "\n",
    "def dtn_ref(nu):\n",
    "    return -k*h1vp(nu,k*a) / hankel1(nu,k*a)\n",
    "\n",
    "A_IE = []\n",
    "B_IE = [] \n",
    "relative_residuals = []\n",
    "ansatz = \"minimalIC\"\n",
    "#ansatz = \"medium\"\n",
    "flags = {\"max_num_iterations\":5000000,\n",
    "         \"use_nonmonotonic_steps\":True,\n",
    "         \"minimizer_progress_to_stdout\":False,\n",
    "         \"num_threads\":4,\n",
    "         \"report_level\":\"Brief\",\n",
    "         \"function_tolerance\":1e-6,\n",
    "         \"parameter_tolerance\":1e-8}\n",
    "A_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)\n",
    "B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)\n",
    "\n",
    "lam = np.array([(l/a)**2 for l in range(Lmax)]) \n",
    "dtn_nr = np.array([ dtn_ref(sqrt(lami)*a) for lami in lam ]) \n",
    "weights = np.array([10**2* abs( hankel1(l,k*a) / hankel1(l,k*r_ext) ) for l in range(Lmax)])\n",
    "final_res = np.zeros(len(lam),dtype=float)\n",
    "\n",
    "l_dtn = opt.learned_dtn(lam,dtn_nr,weights**2)\n",
    "\n",
    "def new_initial_guess(A_old,B_old,ansatz):\n",
    "    N = A_old.shape[0]\n",
    "    A_guess = np.zeros((N+1,N+1),dtype='complex')\n",
    "    B_guess = np.zeros((N+1,N+1),dtype='complex')\n",
    "    if ansatz in [\"medium\",\"full\"]:\n",
    "        A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))\n",
    "        B_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))\n",
    "        A_guess[:N,:N] = A_old[:]\n",
    "        B_guess[:N,:N] = B_old[:]\n",
    "        A_guess[N,N] = 1.0\n",
    "    elif ansatz == \"minimalIC\":\n",
    "        A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))\n",
    "        A_guess[:N,:N] = A_old[:]\n",
    "        B_guess[:N,:N] = B_old[:]\n",
    "        A_guess[N,N] = -100-100j\n",
    "        B_guess[0,N] = 1e-3*(np.random.rand(1) + 1j*np.random.rand(1)) \n",
    "    return A_guess,B_guess\n",
    "\n",
    "Ns = list(range(Nmax))\n",
    "for N in Ns: \n",
    "    l_dtn.Run(A_guess,B_guess,ansatz,flags,final_res)\n",
    "    A_IE.append(A_guess.copy()), B_IE.append(B_guess.copy()),relative_residuals.append(final_res.copy())\n",
    "    A_guess,B_guess = new_initial_guess(A_IE[N],B_IE[N],ansatz)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the learned infinite element matrices $A$ and $B$ have been stored in the lists `A_IE`\n",
    "and `B_IE` according to their number of degrees of freedom $N = 1, \\ldots, $ `Nmax` in radial direction.\n",
    "Next we want to set up a function that solves the scattering problem for a specific finite element `order` \n",
    "for $N = 1, \\ldots, $ `Nmax` and measures the quality of the obtained solution. To this end, we use \n",
    "the reference solution `PlaneWaveHomSolution(k,R,\"sound-soft\")` which is given by (see supplement for derivation)\n",
    "\n",
    "$$\n",
    "u(r,\\varphi) = \\frac{J_{0}(k R)}{ H_{0}^{(1)}(k R)} H_{0}^{(1)}(k r) + \\sum\\limits_{n=1}^{\\infty}{ 2 i^n  \\frac{J_{n}(k R)}{ H_{n}^{(1)}(k R)} H_{n}^{(1)}(k r) \\cos(n \\varphi) }.\n",
    "$$\n",
    "\n",
    "Here $J_{n}$ and $H_{n}^{(1)}$ denote the Bessel and Hankel function of the first kind of order $n$ respectively."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ref_sol = PlaneWaveHomSolution(k,R,\"sound-soft\")\n",
    "bnd_data =  exp(1j*k*x )*k*1j*2*x\n",
    "l2_norm =  sqrt( Integrate ( InnerProduct(ref_sol,ref_sol),mesh,VOL ).real)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function `SolveProblem(order)` consists of three parts:\n",
    "* First the finite element space `X` for the given `order` is created. It consist of a volume FESpace `fes_inner` which is defined on the domain $\\Omega$ and an array `inf_outer` of finite element spaces defined on the coupling boundary $\\Gamma_a$ which realize the tensor product in (1). \n",
    "* Next the linear and bilinear forms for the problem are defined and assembled. The interior variational formulation and the imposition of the inhomogeneous Neumann boundary conditions at the inner boundary follows the standard protocol for finite elements. The exterior part is implemented as described in formula (1).\n",
    "* Finally, the linear system is solved and the relative $L^2$-error on $\\Omega$ with respect to the reference solution is stored in the dictionary `rel_error`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(Draw)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "rel_error = {}\n",
    "\n",
    "def SolveProblem(order):\n",
    "    print(\"Solving for order =\", order)\n",
    "    rel_error_order = []\n",
    "    fes_inner = H1(mesh,complex=True,order=order,dirichlet=\"inner-bnd\")\n",
    "\n",
    "    for N in Ns: \n",
    "        A_N, B_N = A_IE[N], B_IE[N]\n",
    "\n",
    "        fes_surf = Compress(H1(mesh,order=order,complex=True,definedon=mesh.Boundaries(\"outer-bnd\")))\n",
    "        inf_outer = [fes_surf for i in range(A_N.shape[0]-1)]\n",
    "\n",
    "        X = FESpace([fes_inner]+inf_outer)\n",
    "        gfu_X = GridFunction (X)\n",
    "        uX, vX = X.TnT() \n",
    "\n",
    "        f_X = LinearForm (X)\n",
    "\n",
    "        aX = BilinearForm(X, symmetric=False)\n",
    "        aX += (grad(uX[0])*grad(vX[0]) - k**2*uX[0]*vX[0] ) * dx\n",
    "\n",
    "        ds_outbnd = ds(definedon=mesh.Boundaries(\"outer-bnd\"))\n",
    "        for i in range(A_N.shape[0]):\n",
    "            for j in range(A_N.shape[0]):\n",
    "                if abs(A_N[j,i]) > 1e-10 or abs(B_N[j,i]>1e-10):\n",
    "                    aX += ( B_N[j,i]*grad(uX[i]).Trace()*grad(vX[j]).Trace() + A_N[j,i]*uX[i]*vX[j] ) * ds_outbnd\n",
    "\n",
    "        # Setup und solve linear system\n",
    "        gfu_X.components[0].Set(ref_sol,BND)\n",
    "        BVP(aX,f_X,gfu_X)\n",
    "        \n",
    "        rel_error_order.append(sqrt(Integrate(Norm(gfu_X.components[0] - ref_sol)**2,mesh,VOL).real)/l2_norm)\n",
    "        print(\"N = {0},rel l2_err ={1}, memory usage (GB) = {2} \\n \".format(A_N.shape[0]-1,rel_error_order[-1],\n",
    "                                                                            process.memory_info().rss/1e9 ))\n",
    "\n",
    "    # Draw solution for largest N:\n",
    "    rel_error[order] = np.array(rel_error_order)\n",
    "    return Draw(0.25*gfu_X.components[0],mesh,animate=True,deformation=True)\n",
    "\n",
    "draw_handling = SolveProblem(4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#for more powerful computing nodes than the binder cloud:\n",
    "#for order in [6,8,10]:\n",
    "#    draw_handling = SolveProblem(order)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Having solved the problem for different polynomial orders $p$ we can now plot the relative errors for increasing number $N$ of infinite element degrees of freedom."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.rc('legend', fontsize=16)\n",
    "plt.rc('axes', titlesize=16)\n",
    "plt.rc('axes', labelsize=16)\n",
    "plt.rc('xtick', labelsize=14)    \n",
    "plt.rc('ytick', labelsize=14)\n",
    "fig, ax = plt.subplots(figsize=(8,8))\n",
    "for p,err in rel_error.items():\n",
    "    ax.semilogy(Ns,err,label=\"p={0}\".format(p),linewidth=3)\n",
    "ax.legend()\n",
    "ax.set_xlabel(\"N\")\n",
    "ax.set_title(\"Relative error\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
