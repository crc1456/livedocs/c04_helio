#include <solve.hpp>
using namespace ngsolve;
using namespace ngfem;
#include <python_comp.hpp>
#include "Coeff/myCoefficient.hpp"

PYBIND11_MODULE(ngs_refsol,m) {
  // import ngsolve such that python base classes are defined
  auto ngs = py::module::import("ngsolve");
 
  static size_t global_heapsize = 1000000;
  static LocalHeap glh(global_heapsize, "ngs_refsol_lh", true);
  
  ExportReferenceSolution(m);
  ExportPlaneWaveHomSolution(m);
  ExportKiteSolution(m);
  ExportFundamentalSolution(m);
  ExportFundamentalSolution_grad(m);
  ExportEllipticFundamentalSolution(m);
  ExportRealSepSolution(m);
  ExportJumpSolution(m);

  m.def("eval_cf_bnd",[] ( shared_ptr<MeshAccess> ma,
			   ngcomp::Region * definedon,
			   shared_ptr<CoefficientFunction> cf,
			   py::list angles,
			   py::list vals,
			   int aorder
			 ) {
    BitArray mask;
    VorB vb;
    int order = aorder;
    Array<Complex> avals;
    Array<double> angle;
    if (definedon)
      {
        vb = VorB(*definedon);
        mask = BitArray((*definedon).Mask());
      }
    if(!mask.Size()){
      mask = BitArray(ma->GetNRegions(vb));
      mask.Set();
    }

    ma->IterateElements
      (vb, glh, [&] (Ngs_Element el, LocalHeap & lh)
      {
        if(!mask.Test(el.GetIndex())) return;
        auto & trafo = ma->GetTrafo (el, lh);
        IntegrationRule ir(trafo.GetElementType(), order);
        BaseMappedIntegrationRule & mir = trafo(ir, lh);
	FlatMatrix<Complex> values(ir.Size(),cf->Dimension(),lh);
	cf->Evaluate(mir,values);
	for(int i = 0; i < values.Height(); i++) {
	  avals.Append(Complex(values.Row(i)(0)));
	} 	
        for (int i = 0; i < mir.Size(); i++) {
	    angle.Append(atan2(mir[i].GetPoint()(1),mir[i].GetPoint()(0)));
	}
      });
    Array<int> sort(angle.Size());
    for (int i=0; i < sort.Size(); i++) {
      sort[i] = i;
    }
    BubbleSort(angle,sort);
    for (int i=0; i < sort.Size(); i++) {
      angles.append(angle[i]);
      vals.append(avals[sort[i]]);  
    }
  },
  py::arg("mesh"),py::arg("definedon"),py::arg("cf"),py::arg("points"),py::arg("vals"),py::arg("order")  
  );

  // This adds documented flags to the docstring of objects.
  ngs.attr("_add_flags_doc")(m);
}

