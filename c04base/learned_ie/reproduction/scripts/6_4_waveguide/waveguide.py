import ceres_dtn as opt
from ngsolve import *
from netgen.geom2d import SplineGeometry
import numpy as np
from math import pi
from cmath import sqrt as csqrt
import mpmath as mp



a = 2*pi # coupling radius (length of waveguide in infinite direction)
Lmax = 80 # number of considered modes for learning
Nmax = 9  # maximal number of infinite element dofs
Ns = list(range(Nmax+1))
N_waveguide = 33 # how many waveguide modes the reference solution contains
k = 16.5 # wavenumber 
orders = [4,6,8,10] # order of FEM 
show_plots = True # show matplotlib plots

if show_plots:
    import matplotlib.pyplot as plt
    plt.rc('legend',fontsize=14)
    plt.rc('axes',titlesize=16)
    plt.rc('axes',labelsize=16)
    plt.rc('xtick',labelsize=14)
    plt.rc('ytick',labelsize=14)

lam_sample = np.append(np.linspace(0,int(k)**2-10,50),np.linspace(int(k)**2-10,int(k)**2+10,400))
lam_sample = np.append(lam_sample,np.linspace(int(k)**2+10,N_waveguide**2,50))

A_IEs = {} 
B_IEs = {}

ansatz = "minimalIC"
flags = {"max_num_iterations":5000000,
         "use_nonmonotonic_steps":True,
         "minimizer_progress_to_stdout":False,
         "num_threads":4,
         "report_level":"Brief",
         "function_tolerance":1e-14,
         "parameter_tolerance":1e-14,
         "gradient_tolerance":1e-14}

def dtn_ref(lami):
    return -1j*complex( csqrt(k**2-lami ) )

A_IE = []
B_IE = [] 
relative_residuals = []

np.random.seed(seed=123)

A_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)

lam = np.array([l**2 for l in range(Lmax)]) 
weights = [] 
for l in range(Lmax):
    if lam[l].item() <= k**2:
        weights.append(10**2)
    else:
        weights.append(10**2*abs(exp(1j*csqrt(k**2-lam[l]))))
weights = np.array(weights)
l0 = np.max( np.where(weights > 1e-17*weights[0])[0]) 
weights = weights[:l0]
lam = np.array([l**2 for l in range(l0)]) 
dtn_nr = np.array([ dtn_ref(lami) for lami in lam ]) 
dtn_ref_sampled = np.array([ dtn_ref(lami) for lami in lam_sample ]) 

final_res = np.zeros(len(lam),dtype=float)
l_dtn = opt.learned_dtn(lam,dtn_nr,weights**2)
def new_initial_guess(A_old,B_old,ansatz):
    N = A_old.shape[0]
    A_guess = np.zeros((N+1,N+1),dtype='complex')
    B_guess = np.zeros((N+1,N+1),dtype='complex')
    if ansatz in ["medium","full"]:
        A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        B_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        A_guess[:N,:N] = A_old[:]
        B_guess[:N,:N] = B_old[:]
        A_guess[N,N] = 1.0
    elif ansatz == "minimalIC":
        A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        A_guess[:N,:N] = A_old[:]
        B_guess[:N,:N] = B_old[:]
        A_guess[N,N] = (k/16)*(-100-100j)
        B_guess[0,N] = 1e-3*(np.random.rand(1) + 1j*np.random.rand(1)) 
    return A_guess,B_guess

for N in Ns: 
    l_dtn.Run(A_guess,B_guess,ansatz,flags,final_res)
    A_IE.append(A_guess.copy()), B_IE.append(B_guess.copy()),relative_residuals.append(final_res.copy())
    A_guess,B_guess = new_initial_guess(A_IE[N],B_IE[N],ansatz)

A_IEs[k] = A_IE 
B_IEs[k] = B_IE 

plot_collect = [] 
for N in Ns:
    dtn_approx = np.zeros(len(lam_sample),dtype='complex')
    opt.eval_dtn_fct(A_IE[N],B_IE[N],lam_sample,dtn_approx)
    plot_collect.append(dtn_approx)
    print("\n learned poles for N = {0}".format(N))
    learned_poles = np.array([-A_IE[N][j,j] for j in range(1,N+1)])
    learned_poles = learned_poles[np.argsort(learned_poles.imag)]
    for pole in learned_poles:
        print("({0},{1})".format(pole.real,pole.imag))

if show_plots:
    plt.plot(lam_sample,dtn_ref_sampled.real,label="dtn",linewidth=4,color='lightgray')
    for N,lstyle in zip([4,6,8],['solid','dotted','dashed']):
        plt.plot(lam_sample,plot_collect[N].real,linewidth=3,linestyle=lstyle,label="N={0}".format(N))
    plt.plot(lam,dtn_nr.real,label="samples",marker='+',linestyle='None',color='black',markersize=12)
    plt.title("dtn real part, $k =$ {0}".format(k))
    plt.xlabel("$\lambda$")
    plt.legend()
    plt.show() 

    plt.plot(lam_sample,dtn_ref_sampled.imag,label="dtn",linewidth=4,color='lightgray')
    for N,lstyle in zip([4,6,8],['solid','dotted','dashed']):
        plt.plot(lam_sample,plot_collect[N].imag,linewidth=3,linestyle=lstyle,label="N={0}".format(N))
    plt.plot(lam,dtn_nr.imag,label="samples",marker='+',linestyle='None',color='black',markersize=12)
    plt.title("dtn imag part, $k =$ {0}".format(k))
    plt.xlabel("$\lambda$")
    plt.legend()
    plt.show()


# results for real / imaginary part 
plot_collect_real = [lam_sample.real,dtn_ref_sampled.real]
fname_real  = "waveguide-dtn-k{0}-real.dat".format(k)
plot_collect_imag = [lam_sample.real,dtn_ref_sampled.imag]
fname_imag  = "waveguide-dtn-k{0}-imag.dat".format(k)
for N in Ns:
    plot_collect_real.append(plot_collect[N].real) 
    plot_collect_imag.append(plot_collect[N].imag) 
header_str = 'lam dtn'
for N in Ns:
    header_str += " N{0}".format(N)
np.savetxt(fname=fname_real,
           X=np.transpose(plot_collect_real),
           header=header_str,
           comments='')
np.savetxt(fname=fname_imag,
           X=np.transpose(plot_collect_imag),
           header=header_str,
           comments='')

# sample points 
plot_collect = [lam,dtn_nr.real,dtn_nr.imag]
fname  = "waveguide-dtn-k{0}-interp.dat".format(k)
header_str = 'lam real imag'
np.savetxt(fname=fname,
           X=np.transpose(plot_collect),
           header=header_str,
           comments='')

geo = SplineGeometry()
geo.AddRectangle((0,0),(a,pi),bcs=["b","r","t","l"])
mesh = Mesh(geo.GenerateMesh (maxh=0.1,quad_dominated=False))
Draw(mesh)

ref_sol = CoefficientFunction(0.0) 
for l in range(0,N_waveguide+1):
    #print("exp(1j*lambda_ell) = ",exp(1j*a*csqrt(k**2-lam[l])))
    ref_sol += sin(l*y)*exp(1j*csqrt(k**2-lam[l])*x)

Draw(ref_sol,mesh,"ref")
vtk = VTKOutput(ma=mesh,coefs=[ref_sol.real],
                names=["sol"],
                filename="waveguide-refsol-k{0}".format(k),
                subdivision=4)
vtk.Do()

rel_error = {} 
def SolveProblem(order,k):
    
    l2_norm =  sqrt( Integrate ( InnerProduct(ref_sol,ref_sol),mesh,VOL ).real)
    
    rel_error_order = []
    fes_inner = H1(mesh,complex=True,order=order,dirichlet="b|l|t")

    A_IE = A_IEs[k]
    B_IE = B_IEs[k]

    for N in Ns: 
        
        A_N = A_IE[N]
        B_N = B_IE[N]

        fes_surf = H1(mesh,order=order,complex=True,definedon=mesh.Boundaries("r"),dirichlet="b|l|t")
        inf_outer = [fes_surf for i in range(A_N.shape[0]-1)]

        X = FESpace([fes_inner]+inf_outer)

        uX = X.TrialFunction() 
        vX = X.TestFunction() 

        f_X = LinearForm (X)
        f_X.Assemble()
        
        gfu_X = GridFunction (X)
        gfu_X.components[0].Set(ref_sol,BND)
        Draw( gfu_X.components[0],mesh,"approx")
        aX = BilinearForm(X, symmetric=False)
        aX += SymbolicBFI ( grad(uX[0])*grad(vX[0]) - k**2*uX[0]*vX[0] )

        for i in range(A_N.shape[0]):
            for j in range(A_N.shape[0]):
                if abs(A_N[j,i]) > 1e-10 or abs(B_N[j,i]>1e-10):
                    graduX = grad(uX[i]).Trace()
                    gradvX = grad(vX[j]).Trace()  
                    aX += SymbolicBFI ( B_N[j,i]*graduX*gradvX + A_N[j,i]*uX[i]*vX[j]  ,definedon=mesh.Boundaries("r"))
        aX.Assemble()


        res_X = gfu_X.vec.CreateVector()
        res_X.data = f_X.vec - aX.mat * gfu_X.vec
        invX = aX.mat.Inverse(freedofs=X.FreeDofs(), inverse="umfpack")
        gfu_X.vec.data += invX * res_X

        Draw( gfu_X.components[0],mesh,"approx")
        Draw( sqrt(InnerProduct(gfu_X.components[0] - ref_sol,gfu_X.components[0] - ref_sol)) ,mesh,"error")
        
        l2_err = sqrt ( Integrate (  InnerProduct(gfu_X.components[0] - ref_sol,gfu_X.components[0] - ref_sol), mesh, VOL ).real)
        rel_error_order.append(l2_err/l2_norm)
        print("N = {0},rel l2_err ={1} \n ".format(A_N.shape[0]-1,l2_err/l2_norm))
    rel_error[order] = np.array(rel_error_order)

for order in orders: 
    SolveProblem(order,k)

# fixed k, variable order (p)
fname = "waveguide"+"-k{0}.dat".format(k)
results = [np.array(Ns)]
header_str = "N "
for order in orders:
    header_str += "p{0} ".format(order)
    results.append(rel_error[order])
np.savetxt(fname =fname,
           X = np.transpose(results),
           header = header_str,
           comments = '')

