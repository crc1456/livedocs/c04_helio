import numpy as np
from scipy.sparse import diags
from pt_source_shared import problem_data,SolveExteriorProblem

omega = problem_data["wavenumber"]  # wavenumber
a = problem_data["coupling_radius"] # coupling radius
x_source_pos = problem_data["x_source_pos"]  # x position of source
source_labels = problem_data["source_labels"] 
y_source = problem_data["y_source_pos"][0] # y position of source
order = problem_data["order_FEM_coupling_bnd"] # oder of FEM
fac_kappa0 = problem_data["fac_kappa0"] # tuning parameter for HSIE  

Nmax = 44 # maximal number of infinite element dofs
Ns = list(range(Nmax))

A_IEs = {} # for storing results
B_IEs = {} # ... 

# the code for the HSIE matrices has been translated from 
# a corresponding Matlab code originally written by L.Nannen

def diffOp(N_r,a,k0):
    udg = np.arange(1,N_r+1)
    mdg = np.arange(-1,-2*N_r-1-2,-2)
    ldg = udg
    diagonals = [mdg,ldg,udg]
    D = a*diags(np.ones(N_r+1))+(1/(2*1j*k0))*diags(diagonals,[0,-1,1])
    return D

def get_Tm(N_r):
    mdg = np.ones(N_r)
    udg = -np.ones(N_r)
    return diags([mdg,udg],[0,1],shape=(N_r,N_r))

def get_Tp(N_r):
    mdg = np.ones(N_r)
    udg = np.ones(N_r)
    return diags([mdg,udg],[0,1],shape=(N_r,N_r))

def get_HSIEM_matrices(N_r,a,k0):
    Cd = 1/4
    D0long = diffOp(4*N_r,1.0,k0)
    I0long = np.linalg.inv(D0long.todense())
    I0 = I0long[:N_r,:N_r]
    Tp = 0.5*get_Tp(N_r)
    Tm = 0.5*get_Tm(N_r)
    
    L1 = (-2*1j*k0/a)* Tp.transpose()@Tp - (2*Cd*1j/(k0*a))*Tm.transpose()@ I0.transpose() @ I0 @ Tm
    L1[0,0] = L1[0,0] + 1/(2*a)
    L2 = (2*a*1j/k0)*Tm.transpose() @ I0.transpose() @ I0 @ Tm
    L3 = (2*a*1j/k0)*Tm.transpose() @ Tm
    neum_repr = 2*1j*k0*np.ones(N_r)
    neum_repr[0] = 1j*k0-0.5
    return L1,L2,L3,(1/a)*neum_repr

# Setup HSIE matrices for each N 
# (actually independent of source position)
for x_source,fac in zip(x_source_pos,fac_kappa0):
    
    A_IE = []
    B_IE = [] 

    for N in Ns: 
        La,Lb,Lc,neum_repr = get_HSIEM_matrices(N+1,a,omega*a*fac)
        A_HSIEM = La-omega**2*Lc
        B_HSIEM = Lb
        A_IE.append(A_HSIEM.copy()), B_IE.append(B_HSIEM.copy())

    A_IEs[x_source] = A_IE
    B_IEs[x_source] = B_IE

# now use HSIE as transparent boundary condition 
# for the pointsource inside unit disk at x-position x_source
rel_error,ndofs,nzes = SolveExteriorProblem(A_IEs,B_IEs)

# store results in file for plotting
fname = "pt-source-HSIE.dat"
results = [np.array(Ns),np.array(ndofs[x_source_pos[0]]),np.array(nzes[x_source_pos[0]])]
header_str = "N ndofs nzes "
for x_source,label in zip(x_source_pos,source_labels):
    header_str += label +" "
    results.append(rel_error[x_source])
np.savetxt(fname =fname,
            X = np.transpose(results),
            header = header_str,
            comments = '')

