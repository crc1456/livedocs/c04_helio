from ngsolve import *
from netgen.geom2d import SplineGeometry
from ngs_refsol import FundamentalSolution
import numpy as np
from scipy.sparse import diags
ngsglobals.msg_level = 0

problem_data = {}
problem_data["coupling_radius"] = 1.0 # radius of coupling boundary
problem_data["wavenumber"] = 16 # wavenumber of Helmholtz problem
problem_data["x_source_pos"] = [0.95,0.5] # x-position of point source
problem_data["y_source_pos"] = [0.0] # y-position of point source
problem_data["source_labels"] = ["close","far"] # labels for source positions
problem_data["order_FEM_coupling_bnd"] = 10 # order of FEM discretization on coupling boundary
problem_data["solver"] = "umfpack" # solver for linear system
problem_data["fac_kappa0"] = [3,1]  # tuning parameter for HSIE for source positions kappa0 = fac_kappa0*wavenumber*coupling_radius

def SolveExteriorProblem(A_x,B_x):
    rel_error_x = {} # key = x_source
    ndofs_x = {} 
    nzes_x = {}

    y_source = problem_data["y_source_pos"][0]
    omega = problem_data["wavenumber"] 

    geo = SplineGeometry()
    geo.AddCircle( (0,0), problem_data["coupling_radius"], leftdomain=1, rightdomain=0,bc="outer-bnd")
    geo.AddCircle( (0,0), 0.9, leftdomain=0, rightdomain=1,bc="inner-bnd")
    geo.SetMaterial(1, "inner")
    mesh = Mesh(geo.GenerateMesh (maxh=0.05,quad_dominated=False))
    mesh.Curve(problem_data["order_FEM_coupling_bnd"])

    fes_surf1 = H1(mesh,order=problem_data["order_FEM_coupling_bnd"]  ,complex=True,definedon=mesh.Boundaries("outer-bnd"))
    fes_surf = Compress(fes_surf1)

    # determine base nzes for counting (i.e. nonzero elements of mass/stiffness system on coupling boundary)
    a_Gamma = BilinearForm(fes_surf, symmetric=False)
    u_Gamma = fes_surf.TrialFunction() 
    v_Gamma = fes_surf.TestFunction() 
    a_Gamma += SymbolicBFI ( grad(u_Gamma).Trace()*grad(v_Gamma).Trace() + u_Gamma*v_Gamma  ,definedon=mesh.Boundaries("outer-bnd"))
    a_Gamma.Assemble()
    base_nze = a_Gamma.mat.nze
  
    for x_source in problem_data["x_source_pos"]: 

        ref_sol = FundamentalSolution(omega,x_source,y_source,False)
        bnd_data = FundamentalSolution(omega,x_source,y_source,True)

        print("Solving for x:source = {0}".format(x_source))
        l2_norm =  sqrt( Integrate ( InnerProduct(ref_sol,ref_sol),mesh,definedon=mesh.Boundaries("outer-bnd") ).real)
        
        rel_error = []

        ref_sol = FundamentalSolution(omega,x_source,y_source,False)
        bnd_data = FundamentalSolution(omega,x_source,y_source,True)
        
        As = A_x[x_source]
        Bs = B_x[x_source]
        
        nzes = [] 
        ndofs = []

        for A_N,B_N in zip(As,Bs):    
            nze_count = 0            
     
            inf_outer = [fes_surf for i in range(A_N.shape[0])]    
            X = FESpace(inf_outer)
                
            uX = X.TrialFunction() 
            vX = X.TestFunction() 
                
            f_X = LinearForm (X)
            f_X += SymbolicLFI ( -bnd_data*vX[0], definedon=mesh.Boundaries("outer-bnd") )
            f_X.Assemble()
                
            aX = BilinearForm(X, symmetric=False)
                
            for i in range(A_N.shape[0]):
                for j in range(B_N.shape[0]):
                    if (abs(A_N[j,i])>1e-12 or abs(B_N[j,i])>1e-12):
                        nze_count += 1
                        graduX = grad(uX[i]).Trace()
                        gradvX = grad(vX[j]).Trace()  
                        aX += SymbolicBFI ( B_N[j,i]*graduX*gradvX + A_N[j,i]*uX[i]*vX[j]  ,definedon=mesh.Boundaries("outer-bnd"))
            aX.Assemble()
        
            nzes.append(base_nze*nze_count)
            ndofs.append(X.ndof)

            gfu_X = GridFunction (X)    
            res_X = gfu_X.vec.CreateVector()
            gfu_X.vec[:] = 0.0
            res_X.data = f_X.vec - aX.mat * gfu_X.vec
            Xfree = X.FreeDofs()
                
            invX = aX.mat.Inverse(freedofs=X.FreeDofs(), inverse= problem_data["solver"] )
    
            gfu_X.vec.data += invX * res_X
         
            l2_err =   sqrt (Integrate ( InnerProduct(gfu_X.components[0]-ref_sol,gfu_X.components[0]-ref_sol),mesh,definedon=mesh.Boundaries("outer-bnd")  ).real )
            
            print("N = {0},rel l2_err ={1} \n ".format(A_N.shape[0]-1,l2_err/l2_norm))
            rel_error.append(l2_err/l2_norm)

        rel_error_x[x_source] = np.array(rel_error)
        nzes_x[x_source] = nzes
        ndofs_x[x_source] = ndofs

    return rel_error_x,ndofs_x,nzes_x


def P_DoFs(M,test,basis):
    return (M[test,:][:,basis])

from scipy.sparse import kron,csr_matrix
import matplotlib.pyplot as plt
def PlotSparsityPattern(A_N,B_N,name="dummy"):

    geo = SplineGeometry()
    geo.AddCircle( (0,0), problem_data["coupling_radius"], leftdomain=1, rightdomain=0,bc="outer-bnd")
    geo.AddCircle( (0,0), 0.9, leftdomain=0, rightdomain=1,bc="inner-bnd")
    geo.SetMaterial(1, "inner")
    mesh = Mesh(geo.GenerateMesh (maxh=0.05,quad_dominated=False))
    mesh.Curve(problem_data["order_FEM_coupling_bnd"])

    fes_surf1 = H1(mesh,order=problem_data["order_FEM_coupling_bnd"]  ,complex=True,definedon=mesh.Boundaries("outer-bnd"))
    fes_surf = Compress(fes_surf1)

    IF_dof = []
    for dof in range(fes_surf.ndof):
        if fes_surf.FreeDofs()[dof]:
            IF_dof.append(dof)

    # determine base nzes for counting (i.e. nonzero elements of mass/stiffness system on coupling boundary)
    M_Gamma = BilinearForm(fes_surf, symmetric=False)
    K_Gamma = BilinearForm(fes_surf, symmetric=False)
    u_Gamma = fes_surf.TrialFunction() 
    v_Gamma = fes_surf.TestFunction() 
    M_Gamma += SymbolicBFI ( u_Gamma*v_Gamma  ,definedon=mesh.Boundaries("outer-bnd")) # mass matrix
    M_Gamma.Assemble() 
    K_Gamma += SymbolicBFI ( grad(u_Gamma).Trace()*grad(v_Gamma).Trace(),definedon=mesh.Boundaries("outer-bnd")) # stiffness matrix
    K_Gamma.Assemble() 

    rows_M,cols_M,vals_M = M_Gamma.mat.COO()
    M_Gamma_csr = P_DoFs(csr_matrix((vals_M,(rows_M,cols_M))),IF_dof,IF_dof)

    rows_K,cols_K,vals_K = K_Gamma.mat.COO()
    K_Gamma_csr = P_DoFs(csr_matrix((vals_K,(rows_K,cols_K))),IF_dof,IF_dof)

    L_kron = kron(A_N,M_Gamma_csr) + kron(B_N,K_Gamma_csr)
    plt.spy(L_kron,markersize=0.4)
    plt.savefig(name,transparent=True)
    #plt.xlabel("Dofs")
    plt.show()




