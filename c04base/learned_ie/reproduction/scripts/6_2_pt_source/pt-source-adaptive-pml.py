#############################################################################
### Implementation of adaptive PML technique from the paper:             ###
### "An adaptive perfectly matched layer technique for time-harmonic     ###
###  scattering problems",                                               ###
###  Z.Chen and X.Liu, SIAM J.Numer. Anal. 43 (2006)                     ###
###                                                                      ###
###  Additional calculations for our implementation are provided in      ###                                                               
###  in the document "adaptivePML.pdf" included in this folder.          ###
############################################################################

from netgen.geom2d import SplineGeometry
import numpy as np
from ngsolve import *
from ngs_refsol import FundamentalSolution
ngsglobals.msg_level = 0
SetNumThreads(1)
from pt_source_shared import problem_data,SolveExteriorProblem

omega = problem_data["wavenumber"]  # wavenumber
a = problem_data["coupling_radius"] # coupling radius
x_source_pos = problem_data["x_source_pos"]  # x position of source
source_labels = problem_data["source_labels"] 
y_source = problem_data["y_source_pos"][0] # y position of source
solver = problem_data["solver"] # solver for linear system 

order = 6   # order of FEM
n_refs = [13,13]  # number of mesh refinement loops for source positions
bonus_intorder = 20
geom_order = max(10,order) # order for curving the coupling boundary

# PML parameters
eta = 2.5 # problem is discretized on annulus with radii [a,eta]
mm_PML = 2 # profile of PML -> polynomial of degree mm_PML
#sigma0 = 4 # amplitude of PML
sigma0 = 4.5 # amplitude of PML
rad_min = a # start of PML
rad_max = eta # end of PML

r = sqrt(x*x+y*y)

create_screenshot_finest_mesh = False # set true to create screenshot of final mesh
if create_screenshot_finest_mesh:
    import netgen.gui

# define the PML
# see the document "adaptivePML.pdf" in this folder for detailed calculations
definedon_PML = 1
w = rad_max-rad_min
sigma_PML = IfPos(r-rad_min,IfPos(rad_max-r, sigma0*( (r-rad_min)/w )**mm_PML ,0),0)
sigma_hat = IfPos(r-rad_min,IfPos(rad_max-r, (sigma0/(mm_PML+1))*(w/r)*( (r-rad_min)/w )**(mm_PML+1) ,0),0)
alpha_PML = 1 + 1j*sigma_PML
beta_PML = 1 + 1j*sigma_hat
dr_sigma_PML = IfPos(r-rad_min,IfPos(rad_max-r,sigma0*mm_PML*(r-rad_min)**(mm_PML-1)/w**mm_PML,0),0)
dr_sigma_hat = IfPos(r-rad_min,IfPos(rad_max-r,(sigma0/(mm_PML+1))*( (r-rad_min)**mm_PML/w**mm_PML)*(mm_PML*r+rad_min)/r**2,0),0)
dr_alpha_PML = 1j*dr_sigma_PML
dr_beta_PML = 1j*dr_sigma_hat
A_00 = (x/r)**2*(beta_PML/alpha_PML) + (y/r)**2*(alpha_PML/beta_PML)
A_01 = (x*y/r**2)*( beta_PML/alpha_PML - alpha_PML/beta_PML)
A_11 = (x/r)**2*(alpha_PML/beta_PML) + (y/r)**2*(beta_PML/alpha_PML)
A_coeff = CoefficientFunction( (A_00,A_01,A_01,A_11) , dims=(2,2) )
d_quot = (  (dr_beta_PML*r+beta_PML)*alpha_PML - beta_PML*r*dr_alpha_PML ) / alpha_PML**2
alpha_prefac = sqrt(1+sigma_PML**2+sigma0**2+sigma_PML**2*sigma0**2)
rtilde_abs = r*sqrt(1+sigma_hat**2)
w_PML = IfPos(r-rad_min, alpha_prefac*exp(-omega*r*sigma_hat*sqrt(1-r**2/rtilde_abs**2)), 1.0)

physpml_trafo = CoefficientFunction( (x*beta_PML ,
                                      y*beta_PML ) )
physpml_jac = CoefficientFunction((
                                   beta_PML + 1j*(x**2/r)*dr_sigma_hat,
                                   1j*(x*y/r)*dr_sigma_hat,
                                   1j*(x*y/r)*dr_sigma_hat,
                                   beta_PML + 1j*(y**2/r)*dr_sigma_hat 
                                  ),
                                  dims=(2,2) )   
pml_phys = pml.Custom(trafo=physpml_trafo, jac = physpml_jac ) 
eta_tilde = rad_max*(1+1j*(sigma0/(mm_PML+1))*(rad_max-rad_min)/rad_max)
print("omega_hat =", exp(-omega*eta_tilde.imag*sqrt(1- (rad_min/abs(eta_tilde))**2)))

rel_error = {}
ndofs = {}
nzes = {}


for x_source,n_ref in zip(x_source_pos,n_refs):
    
    print("Solving for source at x-position: ", x_source)
    ndof_x = [] 
    l2_errors_x = []
    nzes_x = []

    # reference solution and Neumann data on boundary
    ref_sol = FundamentalSolution(omega,x_source,y_source,False)
    bnd_data = FundamentalSolution(omega,x_source,y_source,True)

    # create (coarse) initial mesh
    geo = SplineGeometry()
    geo.AddCircle( (0,0), a, leftdomain=0, rightdomain=1,bc="outer-bnd")
    geo.AddCircle( (0,0), eta, leftdomain=1,rightdomain=0, bc="infty-bnd")
    geo.SetMaterial(1, "pml")
    mesh = Mesh(geo.GenerateMesh (maxh=1.0,quad_dominated=False)) 
    mesh.Curve(geom_order)

    # FEM space, test- and trialfunctions, Gridfunctions for storing solution
    fes = H1(mesh, complex=True,  order=order,dirichlet=[])
    u,v = fes.TnT()
    gfu = GridFunction (fes)
    udirect = GridFunction (fes)
    
    # Space for boundary indicator 
    V = FacetFESpace(mesh,order = 0) 
    vhat = V.TestFunction() 
    gfindicator = GridFunction(V)

    # define integrators 
    ah = BilinearForm (fes, symmetric=False)
    ah += SymbolicBFI ( grad(u)*grad(v) )
    ah += SymbolicBFI (-omega**2*u*v)

    f_analytic = 0.0
    f = LinearForm (fes)
    f += SymbolicLFI ( -bnd_data*v, definedon=mesh.Boundaries("outer-bnd") ,bonus_intorder=bonus_intorder )
    
    fhat = LinearForm(V)
    fhat += 1*vhat.Trace()*ds(definedon=mesh.Boundaries("outer-bnd"))

    def SolveProblem():
             
        # update spaces
        fes.Update()
        V.Update()
        gfu.Update()
        udirect.Update()  
        gfindicator.Update()
         
        # assemble and solve problem on new mesh
        mesh.SetPML(pml_phys,"pml")
        ah.Assemble() 
        nzes_x.append(ah.mat.nze)
        ndof_x.append(fes.ndof)
        f.Assemble()
        fhat.Assemble()
        mesh.UnSetPML("pml")

        gfindicator.vec[:] = 0.0
        gfindicator.vec.data += fhat.vec
        
        # solve linear system
        gfu.vec[:] = 0.0
        res_direct = gfu.vec.CreateVector()
        udirect.vec[:] = 0.0
        res_direct.data = f.vec - ah.mat * udirect.vec
        invA = ah.mat.Inverse(freedofs=fes.FreeDofs(), inverse=solver)
        udirect.vec.data += invA * res_direct

        # measure relative error 
        l2_norm =  sqrt( Integrate ( ( (ref_sol.real)**2 + (ref_sol.imag)**2 ) ,mesh,definedon=mesh.Boundaries("outer-bnd")))
        l2_err =   sqrt (Integrate ( ( (udirect.real-ref_sol.real )**2 +  (udirect.imag-ref_sol.imag )**2) , mesh, definedon=mesh.Boundaries("outer-bnd")))
        print("ndof = {0}, nze = {1}, volume: rel l2_err ={2} ".format(fes.ndof,ah.mat.nze,l2_err/l2_norm))
        l2_errors_x.append(l2_err/l2_norm)
       
        # estimate error  
        h = specialcf.mesh_size
        n = specialcf.normal(mesh.dim) 
        hesse_u = udirect.Operator("hesse")
        grad_u = grad(udirect)
        lap_u = (  (1/r**2)*d_quot*(x*grad_u[0]+y*grad_u[1]) 
                  +(beta_PML/(alpha_PML*r**2))*(x**2*hesse_u[0,0]+y**2*hesse_u[1,1]+2*x*y*hesse_u[0,1])
                  +(alpha_PML/(beta_PML*r**2))*(-x*grad_u[0]-y*grad_u[1]+y**2*hesse_u[0,0]+x**2*hesse_u[1,1]-2*x*y*hesse_u[0,1])     
                )
         
        # local error estimator as given in equation (3.8) of the cited paper      
        elerr = Integrate( (h**2*InnerProduct(lap_u+alpha_PML*beta_PML*omega**2*udirect,lap_u+alpha_PML*beta_PML*omega**2*udirect))*dx
           +0.5*h*IfPos(gfindicator-1e-12, 
                  2*InnerProduct( n*grad_u - bnd_data,n*grad_u - bnd_data),
                  InnerProduct( n*(A_coeff*grad_u - (A_coeff)*grad_u.Other()),n*(A_coeff*grad_u - (A_coeff*grad_u).Other()))
                  )   
                  *dx(element_boundary=True), 
                  mesh,element_wise=True)
        
        w_prefac = []   
        for el in mesh.Elements():
            # the scaling factor max[ w_PML(x) | x in union of all elements which have non empty intersection with el]
            # is approximated by sampling at nodes and centroids of the corresponding elements
            w_prefac_samples_on_patch = []
            for vertex_el in el.vertices:
                for adjacent_el in mesh[vertex_el].elements:
                    LL = len(mesh[adjacent_el].vertices)
                    c_x = 0 # for x coordinate of centroid of element
                    c_y = 0 
                    for vertex_adjacent in mesh[adjacent_el].vertices:
                        px,py = mesh[vertex_adjacent].point
                        w_prefac_samples_on_patch.append(w_PML(mesh(px,py)))
                        c_x = c_x + px
                        c_y = c_y + py
                    c_x = c_x/LL
                    c_y = c_y/LL
                    w_prefac_samples_on_patch.append(w_PML(mesh(c_x,c_y)))
            w_prefac.append(max(w_prefac_samples_on_patch)) 

        #print("w_prefac = ", w_prefac)
        elerr = [ sqrt(abs(err_T)) * w_T for (err_T,w_T) in zip(elerr,w_prefac)]
        
        # mark elements for refinement 
        sumerr = sum(elerr)
        abs_elerr = [abs(err) for err in elerr]
        maxerr = max(abs_elerr)
        marks = [False for el in mesh.Elements()]
        for el_nr,err in enumerate(abs_elerr):
            if abs(err) > 0.5*maxerr:
                marks[el_nr] = True

        for el in mesh.Elements():        
            mesh.SetRefinementFlag(el,marks[el.nr])
        
        # refine the mesh
        mesh.Refine()
        mesh.Curve(geom_order)

    for i in range(n_ref):
        SolveProblem()

    rel_error[x_source] = l2_errors_x
    ndofs[x_source] = ndof_x
    nzes[x_source] = nzes_x
    
    if create_screenshot_finest_mesh:
        Draw(mesh)
        netgen.gui.Snapshot(w=1000,h=500,filename="adaptive-mesh-{0}.png".format(x_source))
    
#print("ndof = ", ndof)
#print("l2_errors = ", l2_errors)

for x_source,label in zip(x_source_pos,source_labels):
    fname = "pt-source-adaptive-PML-"+label+".dat"
    results = [np.array(ndofs[x_source]),np.array(nzes[x_source])]
    header_str = "ndofs nzes relerr"
    results.append(rel_error[x_source])
    np.savetxt(fname = fname,
           X = np.transpose(results),
           header = header_str,
           comments = '')
