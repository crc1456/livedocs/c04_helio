from netgen.meshing import *
from netgen.csg import *
from netgen.meshing import Mesh as netmesh
from math import pi
import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import csr_matrix
#from scipy.optimize import minimize,root,least_squares
#import scipy.sparse.linalg
from ngsolve import Mesh as NGSMesh

from compute_pot import local_polynomial_estimator,logder

#np.random.seed(seed=99)
np.random.seed(seed=1)

from ngsolve import *

import ceres_dtn as opt

ngsglobals.msg_level = 0
SetNumThreads(4)

#################################################### 
# parameters
####################################################

order_ODE = 8 # order of ODE discretization
L_max = 6000 # how many DtN numbers to take
f_hz = 0.007 # frequency 
a = 1.0 # coupling radius 
L_spacing = 100 # take only DtN numbers with index % L_spacing == 0 
spline_order = 6 # order of Spline approx. for coefficients
R_max_ODE = 1.0033 # to how far out the ODE should be solved
Nmax = 6 # maximal dofs for learned IE in radial direction
Nrs = list(range(Nmax))
show_plots = False
ansatz = "minimalIC"
flags = {"max_num_iterations":5000000,
         "check_gradients":False, 
         "use_nonmonotonic_steps":True,
         "minimizer_progress_to_stdout":False,
         "num_threads":4,
         "report_level":"Brief",
         "function_tolerance":1e-10,
         "parameter_tolerance":1e-10}
nodamping  = False
sample_rr = np.linspace(a,R_max_ODE,400)
sample_r = sample_rr.tolist()
l_eval = [10,1000,5000]
eval_lami = [l*(l+1)/a**2 for l in l_eval ]

#################################################### 
# some auxiliary functions
####################################################

def sqrt_2branch(z):
    if z.real < 0 and z.imag <0:
        return - sqrt(z)
    else:
        return sqrt(z)

def P_DoFs(M,test,basis):
    return (M[test,:][:,basis])

def Make1DMesh_flex(R_min,R_max,N_elems=50):

    mesh_1D = netmesh(dim=1)
    pids = []   
    delta_r = (R_max-R_min)/N_elems
    for i in range(N_elems+1):
        pids.append (mesh_1D.Add (MeshPoint(Pnt(R_min + delta_r*i, 0, 0))))                     
    n_mesh = len(pids)-1
    for i in range(n_mesh):
        mesh_1D.Add(Element1D([pids[i],pids[i+1]],index=1))
    mesh_1D.Add (Element0D( pids[0], index=1))
    mesh_1D.Add (Element0D( pids[n_mesh], index=2))
    mesh_1D.SetBCName(0,"left")
    mesh_1D.SetBCName(1,"right")
    mesh_1D = NGSMesh(mesh_1D)    
    return mesh_1D

def new_initial_guess(A_old,B_old,ansatz):
    N = A_old.shape[0]
    A_guess = np.zeros((N+1,N+1),dtype='complex')
    B_guess = np.zeros((N+1,N+1),dtype='complex')
    if ansatz in ["medium","full"]:
        A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        B_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        A_guess[:N,:N] = A_old[:]
        B_guess[:N,:N] = B_old[:]
        A_guess[N,N] = 1.0
    elif ansatz == "minimalIC":
        A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        A_guess[:N,:N] = A_old[:]
        B_guess[:N,:N] = B_old[:]
        A_guess[N,N] = -1e-7*(-1)**N + 0.1j
        #l1_guess[N,N] = 1e+9
        #l1_guess[N,N] = (np.random.rand(1) + 1j*np.random.rand(1))
        #l2_guess[0,N] = (np.random.rand(1) + 1j*np.random.rand(1))
    else:
        A_guess[:N,:N] = A_old[:]
        B_guess[:N,:N] = B_old[:]
        A_guess[N,N] = -100-100j
        A_guess[0,N] = A_guess[N,0] = 10**(3/2)*(np.random.rand(1) + 1j*np.random.rand(1))
    return A_guess,B_guess

#################################################### 
# damping model
####################################################

RSun = 6.963e8 # radius of the Sun in m 
omega_f = f_hz*2*pi*RSun
gamma0 = 2*pi*4.29*1e-6*RSun
omega0 = 0.003*2*pi*RSun
if f_hz < 0.0053:
    gamma = gamma0*(omega_f/omega0)**(5.77)
else:
    gamma = gamma0*(0.0053*2*pi*RSun/omega0)**(5.77)
if nodamping:
    gamma = 0 
omega_squared = omega_f**2+2*1j*omega_f*gamma

#################################################### 
# load solar models and compute potential
####################################################

rS,cS,rhoS,_,__,___ = np.loadtxt("modelSinput.txt",unpack=True)
rV,vV,rhoV,Tv  = np.loadtxt("VALCinput.txt",unpack=True)

# sound speed is not given in VAL-C model
# calculate it from temperature using the ideal gas law
gamma_ideal = 5/3 # adiabatic index
mu_ideal = 1.3*10**(-3) # mean molecular weight 
R_gas = 8.31446261815324 # universal gas constant 
cV = np.sqrt(gamma_ideal*R_gas*Tv/mu_ideal)

# scale to SI units
cS = (10**-2)*cS
rhoS = (10**3)*rhoS
rhoV = (10**3)*rhoV

# overlap interval
rmin = rV[-1]
rmax = rS[0]

weightS = np.minimum(1, (rmax-rS)/(rmax-rmin))
weightV = np.minimum(1, (rV-rmin)/(rmax-rmin))

r = np.concatenate((rS,rV)) 
ind = np.argsort(r)
r = r[ind]
c = np.concatenate((cS,cV))[ind]
rho = np.concatenate((rhoS,rhoV))[ind]
weight = np.concatenate((weightS,weightV))[ind]

RSun = 6.963e8 # radius of the Sun in m
c0 = 6.855e5 # sound speed at interface [cm/s]
H = 1.25e7 # density scale height in [cm]
omega_c = 0.0052*2*pi # c0/(2*H) cut-off frequency in Herz

rho_clean = logder(rho,r,weight,1)
c_clean = logder(c,r,weight,1)
i0 = np.min( np.nonzero(r > 0.99)[0] ) 

f,df,ddf = logder(1/np.sqrt(rho),r,weight,3)
pot_rho = np.sqrt(rho_clean)*(ddf+2*df/r)
pot_c = c_clean

#################################################### 
# BSpline approximation of c,rho and potential 
####################################################

r_beg = r[0]
pot_rho_beg = pot_rho[0]
pot_c_beg = pot_c[0]
r_end = r[-1]
pot_rho_end = pot_rho[-1]
pot_c_end = pot_c[-1]

r = np.append([r_beg for i in range(spline_order)],r)
pot_rho = np.append([pot_rho_beg for i in range(spline_order)],pot_rho)
pot_c = np.append([pot_c_beg for i in range(spline_order)],pot_c)

r = np.append(r,[r_end for i in range(spline_order)])
pot_rho = np.append(pot_rho,[pot_rho_end for i in range(spline_order)])
pot_c = np.append(pot_c,[pot_c_end for i in range(spline_order)])

r = r.tolist()
pot_rho = pot_rho.tolist()
pot_c = pot_c.tolist()

pot_rho_B = BSpline(spline_order,r,pot_rho)(x)
pot_c_B = BSpline(spline_order,r,pot_c)(x)

#pot_1D = pot_rho_B - omega**2/pot_c_B**2
pot_1D = pot_rho_B - omega_squared/pot_c_B**2

#################################################### 
# Compute DtN numbers by solving ODEs
####################################################

mesh_1D = Make1DMesh_flex(a,R_max_ODE,100)
sample_r_pts = [mesh_1D(r_pt) for r_pt in sample_r]
fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[1])
u_DtN,v_DtN = fes_DtN.TnT()
gfu_DtN = GridFunction (fes_DtN)

f_DtN = LinearForm(fes_DtN)
f_DtN.Assemble()
r_DtN = f_DtN.vec.CreateVector()
frees_DtN = [i for i in range(1,fes_DtN.ndof)]

DtN_continuous_eigvals = [] 
filtered_lam_all = []
DtN_ODE_filtered_all = [] 

lam_continuous = np.array([l*(l+1)/a**2 for l in np.arange(L_max) ] ) 


def get_ODE_DtN(lam,DtN_list,save_modes=False):
     
    plot_modes_collect = [sample_rr]
    header_modes = "r"
    for lami in lam:    
        
        a_DtN = BilinearForm (fes_DtN, symmetric=False)
        a_DtN += SymbolicBFI((x**2)*grad(u_DtN)*grad(v_DtN)) 
        a_DtN += SymbolicBFI(( lami.item()*a**2 +  pot_1D*x**2 )*u_DtN*v_DtN)
        a_DtN.Assemble()

        gfu_DtN.vec[:] = 0.0
        gfu_DtN.Set(1.0, BND)
        r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
        gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs()) * r_DtN
       
        if save_modes and lami in eval_lami:
            l_idx = eval_lami.index(lami)
            eval_gfu = np.array([gfu_DtN(mpt) for mpt in sample_r_pts])
            plot_modes_collect.append(eval_gfu.real.copy())
            header_modes += " l{0}".format(l_eval[l_idx])
            if show_plots:
                print("l =", l_eval[l_idx])
                plt.plot( sample_rr,  eval_gfu.real )
                plt.title("Real part of dtn mode l = {0}".format(l_eval[l_idx]))
                plt.show()

        rows_DtN,cols_DtN,vals_DtN = a_DtN.mat.COO()
        A_DtN = csr_matrix((vals_DtN,(rows_DtN,cols_DtN)))
        val1 = P_DoFs(A_DtN,[0],frees_DtN).dot(gfu_DtN.vec.FV().NumPy()[frees_DtN])
        val2 = P_DoFs(A_DtN,[0],[0]).dot(gfu_DtN.vec.FV().NumPy()[0])
        val = -P_DoFs(A_DtN,[0],frees_DtN).dot(gfu_DtN.vec.FV().NumPy()[frees_DtN])-P_DoFs(A_DtN,[0],[0]).dot(gfu_DtN.vec.FV().NumPy()[0])

        DtN_list.append(-val.item()/a**2)
        # divide by a**2 to get unscaled normal derivative
    
    if save_modes: 
        fname_eig = "VALC-modes-{0}Hz.dat".format(f_hz)
        np.savetxt(fname=fname_eig,
               X=np.transpose(plot_modes_collect), 
               header = header_modes,
               comments='' 
              )


print("Compute reference DtN numbers by solving ODEs")
get_ODE_DtN(lam_continuous,DtN_continuous_eigvals,True)

if show_plots:
    plt.plot(np.arange(L_max) ,np.array(DtN_continuous_eigvals).real,label='real' )
    plt.title("analytic dtn function")
    plt.show()

fname_ref = "dtn-VALC-ref-{0}Hz.dat".format(f_hz)
np.savetxt(fname=fname_ref,
           X=np.transpose([ np.arange(L_max), np.array(DtN_continuous_eigvals).real,np.array(DtN_continuous_eigvals).imag  ]), 
           header = 'l zetaReal zetaImag',
           comments='' 
          )

#################################################### 
# Filter out DtN numbers: 
# take only those l which either fulfill
# - l % L_spacing == 0 (equidistant sampling to 
#   cover general behavior of dtn function
# - those l where the dtn function has a large 
#   gradient ( grad_dtn > 5*np.average(grad_dtn))
####################################################

dtn_a = np.array(DtN_continuous_eigvals)
lam_a = np.array(lam_continuous)
diff_dtn = dtn_a[1:] - dtn_a[:-1]
diff_lam = lam_a[1:] - lam_a[:-1]
grad_dtn = np.abs( diff_dtn / diff_lam) 
#print("np.average(grad_dtn) = ", np.average(grad_dtn) )
if gamma == 0:
    ind_large = np.nonzero(grad_dtn > 5*np.average(grad_dtn ))[0]
else:
    ind_large = np.nonzero(grad_dtn > 3*np.average(grad_dtn ))[0]
#print("ind_larg = ", ind_large)
ind_large = 1 + ind_large 

if show_plots:
    plt.plot(np.arange(L_max)[1:] ,grad_dtn,label='grad' )
    plt.plot(np.arange(L_max)[ind_large] ,grad_dtn[ind_large],label='selected idx',marker='.' )
    plt.legend()
    plt.show()

L_filter = [] 
w_weight = [] 
alpha_decay = 8*L_spacing
for l in np.arange(L_max):
    #if l in list(range(3370,3400)) or l in list(range(5490,5530)) or l in list(range(7412,7430)):
    if l in ind_large:
        L_filter.append(l)
        w_weight.append(exp(-l/alpha_decay))
    else:
        if l % L_spacing == 0:
            L_filter.append(l)
            w_weight.append(exp(-l/alpha_decay))

print("len(L_filter)", len(L_filter))
#print("L_filter = ", L_filter)
#print("w_weight = ", w_weight)
w_weight = np.array(w_weight)

filtered_lam_all = np.array(lam_continuous)[L_filter]
DtN_ODE_filtered_all = np.array(DtN_continuous_eigvals)[L_filter]

#fname_interp = "dtn-VALC-interp-{0}Hz.dat".format(f_hz)
#np.savetxt(fname=fname_interp,
#           X=np.transpose([filtered_lam_all.real, DtN_ODE_filtered_all.real ]), 
#           header = 'lam zeta',
#           comments='' 
#          )

#################################################### 
# Solve the minimization problem
####################################################

lam = filtered_lam_all
dtn_ref = DtN_ODE_filtered_all
weights = w_weight

A_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
l_dtn = opt.learned_dtn(lam,dtn_ref,weights**2)

zeta_approx_evals = []

A_IE = []
B_IE = []

for N in Nrs:
 
    l_dtn.Run(A_guess,B_guess,ansatz,flags)
    A_IE.append(A_guess.copy()), B_IE.append(B_guess.copy())
    A_guess,B_guess = new_initial_guess(A_IE[N],B_IE[N],ansatz)

    if ansatz in ["medium","minimalIC"]:
        learned_poles = [-A_IE[N][i,i]/B_IE[N][i,i] for i in range(1,A_IE[N].shape[0])]
        print("learned_poles = ", learned_poles)

    zeta_approx_plot = np.zeros(len(lam),dtype=complex)
    opt.eval_dtn_fct(A_IE[N],B_IE[N],np.array(lam,dtype=float),zeta_approx_plot)
    zeta_approx_evals.append(zeta_approx_plot)
 
    if show_plots:
        plt.plot(filtered_lam_all,DtN_ODE_filtered_all.real ,label="analytic")
        plt.plot(filtered_lam_all,zeta_approx_plot.real,label="app",linestyle="dashed")
        plt.xlabel("$\lambda$")
        plt.title("zeta")
        plt.legend()
        plt.show()

plot_collect_real = [ np.array(L_filter) ]
plot_collect_imag = [ np.array(L_filter) ]
header_str = 'l'
for N_r,zeta_approx_plot in zip(Nrs,zeta_approx_evals):
    header_str += " N{0}".format(N_r)
    plot_collect_real.append(zeta_approx_plot.real)
    plot_collect_imag.append(zeta_approx_plot.imag)

fname_learned_real ="learned-dtn-VALC-real.dat"
fname_learned_imag ="learned-dtn-VALC-imag.dat"

np.savetxt(fname=fname_learned_real,
           X=np.transpose(plot_collect_real),
           header=header_str,
           comments='')
np.savetxt(fname=fname_learned_imag,
           X=np.transpose(plot_collect_imag),
           header=header_str,
           comments='')

#################################################### 
# Compute poles of dtn 
####################################################
from GRPF import Poles_and_Roots 
from cmath import sqrt as csqrt

def fun(z): 
    DtN_list = [] 
    get_ODE_DtN([z],DtN_list)
    return DtN_list[0]

param = {} # for searching poles
param["Tol"] = 1e-9
param["visual"] = 1 
param["ItMax"] = 25
param["NodesMax"] = 500000
param["xrange"] = [100*(100+1),6000*(6000+1)]
param["yrange"] = [-200000,1000000]
param["h"] = 50000
result = Poles_and_Roots(fun,param)

def lam_to_ell(z):
    return -0.5+csqrt( 0.25+a**2*z)

poles = result["poles"] 
print("poles of dtn function")
for pole in poles:
    print("l = {0}".format(lam_to_ell(pole) ))

print("learned poles:")
for N in Nrs:
    print("N = {0}".format(N))
    learned_poles = [-A_IE[N][i,i]/B_IE[N][i,i] for i in range(1,A_IE[N].shape[0])]
    for pole in learned_poles:
        print("l = {0}".format(lam_to_ell(pole)))
