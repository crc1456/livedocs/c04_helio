# pole_finder

Routines for locating poles (and roots) of the dtn function. The implementation is based on the articles 
and `MATLAB` code given below. 

# References:
- `P. Kowalczyk`, "Complex Root Finding Algorithm Based on Delaunay Triangulation", ACM Transactions on Mathematical Software, vol. 41, no. 3, art. 19, pp. 1-13, June 2015, [link](https://dl.acm.org/citation.cfm?id=2699457)
-  `P. Kowalczyk`, "Global Complex Roots and Poles Finding Algorithm Based on Phase Analysis for Propagation and Radiation Problems" IEEE Transactions on Antennas and Propagation, vol. 66, no. 12, pp. 7198-7205, Dec 2018, [link](https://ieeexplore.ieee.org/document/8457320)
- Matlab code available from [github](https://github.com/PioKow/GRPF)  

# Installation 
`python3 setup.py install --user`

# Test case 
`python3 test/simple_rational_function.py`.
