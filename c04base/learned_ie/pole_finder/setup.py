#!/usr/bin/env python

from setuptools import setup,find_packages

setup(
    name='pole_finder',
    version='0.0.1',
    author='JP',
    author_email='janosch.preuss@stud.uni-goettingen.de',
    description='Routine for locating poles and roots of meromorphic functions',
    long_description='',
    packages=find_packages(),
    zip_safe=False,
)
