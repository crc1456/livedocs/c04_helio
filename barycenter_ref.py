def barycenter_ref(mesh):
    from netgen.meshing import Mesh,FaceDescriptor,Element1D,Pnt,MeshPoint,Element2D
    # from netgen.csg import *

    from ngsolve import VERTEX
    pn = mesh.GetPeriodicNodePairs(VERTEX)
    mesh = mesh.ngmesh
    newmesh = Mesh()
    newmesh.dim = 2
    point_ids = [newmesh.Add(p) for p in mesh.Points()]

    if len(pn):
        for m,i in pn:
            newmesh.AddPointIdentification(m[0]+1,m[1]+1,identnr=i+1,type=2)

    def average_pnt(lv):
        l = len(lv)
        xd_coord = [0,0]
        for d in [0,1]:
            xd_coord[d] = sum([mesh.Points()[v].p[d] for v in lv]) / l
        return Pnt(xd_coord[0], xd_coord[1], 0.0)

    FD = FaceDescriptor(surfnr=1,domin=1,domout=0,bc=1)
    # FD.bcname="bnd"
    newmesh.Add ( FD )
    # import pdb; pdb.set_trace()
    # newmesh.SetBCName(1,'bnd')
    # newmesh.Add (mesh.FaceDescriptor(1))
    #newmesh.SetMaterial(1, "mat")

    for edge in mesh.Elements1D():
        v1,v2 = edge.vertices
        newmesh.Add(Element1D(vertices=[v1,v2], index=edge.index))
    newmesh.SetBCName(0,'bnd')
    newmesh.SetBCName(1,'bnd')
    newmesh.SetBCName(2,'bnd')
    newmesh.SetBCName(3,'bnd')

    for el in mesh.Elements2D(): #print(i);
        v1,v2,v3 = el.vertices
        cellcenter = average_pnt(el.vertices)
        v123 = newmesh.Add( MeshPoint(cellcenter) )
        newmesh.Add(Element2D(index=el.index, vertices = [v1,v2,v123]))
        newmesh.Add(Element2D(index=el.index, vertices = [v3,v1,v123]))
        newmesh.Add(Element2D(index=el.index, vertices = [v2,v3,v123]))

    from ngsolve import Mesh
    return Mesh(newmesh)

def MakeXMesh(h=1,periodic=False):
    from ngsolve.meshes import MakeStructured2DMesh
    mesh = MakeStructured2DMesh( quads = True , nx =2**(h+1), ny =2**(h+1), mapping = lambda x,y: (8*x-4,8*y-4), periodic_x=periodic, periodic_y=periodic)
    from netgen.meshing import Mesh,FaceDescriptor,Element1D,Pnt,MeshPoint,Element2D
    # from netgen.csg import *

    from ngsolve import VERTEX
    pn = mesh.GetPeriodicNodePairs(VERTEX)
    mesh = mesh.ngmesh
    newmesh = Mesh()
    newmesh.dim = 2
    point_ids = [newmesh.Add(p) for p in mesh.Points()]

    if len(pn):
        for m,i in pn:
            newmesh.AddPointIdentification(m[0]+1,m[1]+1,identnr=i+1,type=2)

    def average_pnt(lv):
        l = len(lv)
        xd_coord = [0,0]
        for d in [0,1]:
            xd_coord[d] = sum([mesh.Points()[v].p[d] for v in lv]) / l
        return Pnt(xd_coord[0], xd_coord[1], 0.0)

    newmesh.Add (FaceDescriptor(surfnr=1,domin=1,domout=0,bc=1))
    # newmesh.Add (mesh.FaceDescriptor(1))
    #newmesh.SetMaterial(1, "mat")

    for edge in mesh.Elements1D():
        v1,v2 = edge.vertices
        newmesh.Add(Element1D(vertices=[v1,v2], index=edge.index))

    for el in mesh.Elements2D(): #print(i);
        v1,v2,v3,v4 = el.vertices
        cellcenter = average_pnt(el.vertices)
        v1234 = newmesh.Add( MeshPoint(cellcenter) )
        newmesh.Add(Element2D(index=el.index, vertices = [v1,v2,v1234]))
        newmesh.Add(Element2D(index=el.index, vertices = [v2,v3,v1234]))
        newmesh.Add(Element2D(index=el.index, vertices = [v3,v4,v1234]))
        newmesh.Add(Element2D(index=el.index, vertices = [v4,v1,v1234]))

    from ngsolve import Mesh
    return Mesh(newmesh)

#newmesh.SetGeometry(unit_square)
# newmesh.Save("test.vol")
# newmesh.Save("test.vol.gz")

#########################################################


if __name__ == "__main__":
    from netgen.geom2d import unit_square
    from ngsolve import Mesh
    mesh = Mesh(unit_square.GenerateMesh(maxh=0.3))
    mesh = barycenter_ref(mesh)
    # mesh = Mesh()
    # mesh.Load("test.vol.gz")

    from ngsolve import *
    import netgen.gui
    # mesh = Mesh(mesh)
    # mesh = MakeXMesh()
    Draw(mesh)
    input()
