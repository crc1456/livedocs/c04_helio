#classic packages
import numpy as np
import pandas as pd
from math import pi 
from ngsolve import log, exp, sqrt 
import pickle

#Formatted outputs
def print_green(s):
    print("\033[92m{}\033[00m".format(s))

def print_red(s):
    print("\033[91m{}\033[00m".format(s))

def print_yellow(s):
    print("\033[93m{}\033[00m".format(s))

def linebreaker(): 
    print("#####################################################")


#Estimate order of convergence
def get_eoc(error,lasterror,exp_order): 
    eoc = log(lasterror/error)/log(2)
    eoc = round(eoc,3)
    print_green("Theoretical order of convergence = {}".format(exp_order))
    print_red("Estimated order of convergence = {}".format(eoc))
    return eoc


#Special functions
def gaussian(x,y,alpha,x0=0,y0=0): 
    g = sqrt(alpha/pi)*exp( -alpha*((x-x0)**2+(y-y0)**2))
    return g 


#For sampling the unit circle 
def SampleUnitCircle(nsample): 
    #https://stackoverflow.com/questions/5837572/generate-a-random-point-within-a-circle-uniformly
    t = 2 * np.pi*np.random.uniform(0,1,nsample)
    u = np.random.uniform(0,1,nsample) + np.random.uniform(0,1,nsample)
    r = [2-u[i] if u[i] > 1 else u[i] for i in range(len(u))]
    xpts = r*np.cos(t)
    ypts = r*np.sin(t)
    return xpts,ypts

#for estimating the Mach number: 
def estimateMachNumberUnitCircle(flow,mesh): 
    xpts,ypts = SampleUnitCircle(100000)
    mip = mesh(xpts,ypts)
    maxval = np.max(np.abs(flow(mip)))
    return maxval**2



